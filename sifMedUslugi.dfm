inherited frmMedUslugii: TfrmMedUslugii
  Caption = #1055#1072#1082#1077#1090#1080
  ClientWidth = 766
  ExplicitWidth = 782
  PixelsPerInch = 96
  TextHeight = 13
  inherited lPanel: TPanel
    Width = 766
    Height = 346
    ExplicitWidth = 766
    ExplicitHeight = 378
    inherited cxGrid1: TcxGrid
      Width = 762
      Height = 342
      ExplicitWidth = 762
      ExplicitHeight = 374
      inherited cxGrid1DBTableView1: TcxGridDBTableView
        OnDblClick = cxGrid1DBTableView1DblClick
        DataController.DataSource = dm.dsMEDUslugi
        OptionsSelection.MultiSelect = True
        object cxGrid1DBTableView1ID: TcxGridDBColumn
          DataBinding.FieldName = 'ID'
          Width = 200
        end
        object cxGrid1DBTableView1RE: TcxGridDBColumn
          DataBinding.FieldName = 'RE'
          Width = 200
        end
        object cxGrid1DBTableView1NAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'NAZIV'
          Width = 200
        end
        object cxGrid1DBTableView1BESPLATNA: TcxGridDBColumn
          DataBinding.FieldName = 'BESPLATNA'
          Width = 200
        end
        object cxGrid1DBTableView1CENA_FZO: TcxGridDBColumn
          DataBinding.FieldName = 'CENA_FZO'
          Width = 200
        end
        object cxGrid1DBTableView1RE_NAPLATA: TcxGridDBColumn
          DataBinding.FieldName = 'RE_NAPLATA'
          Width = 200
        end
        object cxGrid1DBTableView1SIFRA: TcxGridDBColumn
          DataBinding.FieldName = 'SIFRA'
          Width = 200
        end
        object cxGrid1DBTableView1DEF_ANALIZA: TcxGridDBColumn
          DataBinding.FieldName = 'DEF_ANALIZA'
          Width = 200
        end
        object cxGrid1DBTableView1POSTAPKA: TcxGridDBColumn
          DataBinding.FieldName = 'POSTAPKA'
          Width = 200
        end
        object cxGrid1DBTableView1AKTIVNA: TcxGridDBColumn
          DataBinding.FieldName = 'AKTIVNA'
          Width = 200
        end
        object cxGrid1DBTableView1BR_ANALIZI: TcxGridDBColumn
          DataBinding.FieldName = 'BR_ANALIZI'
          Width = 200
        end
        object cxGrid1DBTableView1GRUPA: TcxGridDBColumn
          DataBinding.FieldName = 'GRUPA'
          Width = 200
        end
        object cxGrid1DBTableView1KRATENKA: TcxGridDBColumn
          DataBinding.FieldName = 'KRATENKA'
          Width = 200
        end
        object cxGrid1DBTableView1PRIMEROK: TcxGridDBColumn
          DataBinding.FieldName = 'PRIMEROK'
          Width = 200
        end
        object cxGrid1DBTableView1GRUPA_CENA: TcxGridDBColumn
          DataBinding.FieldName = 'GRUPA_CENA'
          Width = 200
        end
      end
    end
  end
  inherited dPanel: TPanel
    Top = 472
    Width = 766
    Height = 58
    ExplicitTop = 472
    ExplicitWidth = 766
    ExplicitHeight = 58
    inherited Label1: TLabel
      Left = 17
      Top = 22
      Visible = False
      ExplicitLeft = 17
      ExplicitTop = 22
    end
    inherited Sifra: TcxDBTextEdit
      Left = 69
      Top = 19
      Hint = #1064#1080#1092#1088#1072' '#1085#1072' '#1087#1072#1082#1077#1090
      DataBinding.DataField = 'ID'
      DataBinding.DataSource = dm.dslSANPaketi
      Visible = False
      ExplicitLeft = 69
      ExplicitTop = 19
    end
    inherited OtkaziButton: TcxButton
      Left = 675
      Top = 18
      Visible = False
      ExplicitLeft = 675
      ExplicitTop = -14
    end
    inherited ZapisiButton: TcxButton
      Left = 594
      Top = 18
      Visible = False
      ExplicitLeft = 594
      ExplicitTop = -14
    end
  end
  inherited dxRibbon1: TdxRibbon
    Width = 766
    ExplicitWidth = 766
    inherited dxRibbon1Tab1: TdxRibbonTab
      Index = 0
    end
    inherited dxRibbon1Tab2: TdxRibbonTab
      Index = 1
    end
  end
  inherited StatusBar1: TdxRibbonStatusBar
    Width = 766
    Panels = <
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = 'Enter '#1080#1083#1080' '#1044#1074#1086#1077#1085' '#1082#1083#1080#1082' - '#1047#1072#1087#1080#1096#1080', Esc - '#1054#1090#1082#1072#1078#1080' / '#1048#1079#1083#1077#1079
      end>
    ExplicitWidth = 766
  end
  inherited PopupMenu1: TPopupMenu
    Top = 216
  end
  inherited dxBarManager1: TdxBarManager
    Left = 448
    Top = 240
    PixelsPerInch = 96
    inherited dxBarManager1Bar1: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 69
      FloatClientHeight = 167
      Visible = False
    end
    inherited dxBarManager1Bar2: TdxBar
      DockedLeft = 0
      FloatClientWidth = 149
      FloatClientHeight = 189
    end
    inherited dxBarManager1Bar3: TdxBar
      DockedLeft = 320
      FloatClientWidth = 59
      FloatClientHeight = 108
    end
    inherited dxBarManager1Bar4: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 141
      FloatClientHeight = 221
    end
    inherited dxBarManager1BarTabela: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 174
      FloatClientHeight = 113
    end
    object dxBarLargeButton10: TdxBarLargeButton
      Action = aDefinicijaPaket
      Category = 0
      ScreenTip = tipDefinicijaPaket
    end
    object dxBarLargeButton11: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
  end
  inherited ActionList1: TActionList
    object aDefinicijaPaket: TAction
      Caption = #1044#1077#1092#1080#1085#1080#1094#1080#1112#1072' '#1085#1072' '#1087#1072#1082#1077#1090
      ImageIndex = 66
    end
    object aPrevzemiPaketi: TAction
      Caption = #1055#1088#1077#1074#1079#1077#1084#1080' '#1075#1080' '#1089#1077#1083#1077#1082#1090#1080#1088#1072#1085#1080#1090#1077' '#1087#1072#1082#1077#1090#1080
      ImageIndex = 62
    end
  end
  inherited dxComponentPrinter1: TdxComponentPrinter
    inherited dxComponentPrinter1Link1: TdxGridReportLink
      ReportDocument.CreationDate = 41330.419730601850000000
      Styles.BandHeader = nil
      Styles.Caption = nil
      Styles.CardCaptionRow = nil
      Styles.CardRowCaption = nil
      Styles.Content = nil
      Styles.ContentEven = nil
      Styles.ContentOdd = nil
      Styles.FilterBar = nil
      Styles.Footer = nil
      Styles.Group = nil
      Styles.Header = nil
      Styles.Preview = nil
      Styles.Selection = nil
      BuiltInReportLink = True
    end
  end
  inherited dxBarScreenTipRepository1: TdxBarScreenTipRepository
    Left = 696
    Top = 208
    PixelsPerInch = 96
    object tipDefinicijaPaket: TdxScreenTip
      Header.Text = #1044#1077#1092#1080#1085#1080#1094#1080#1112#1072' '#1085#1072' '#1087#1072#1082#1077#1090
      Description.Text = #1044#1077#1092#1080#1085#1080#1088#1072#1114#1077' '#1085#1072' '#1091#1089#1091#1075#1080#1090#1077' '#1082#1086#1080' '#1089#1087#1072#1107#1072#1072#1090' '#1074#1086' '#1089#1077#1083#1077#1082#1090#1080#1088#1072#1085#1080#1086#1090' '#1087#1072#1082#1077#1090#13#10
    end
  end
end
