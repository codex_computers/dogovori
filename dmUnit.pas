unit dmUnit;

interface

uses
  System.SysUtils, System.Classes, Data.DB, FIBDataSet, pFIBDataSet, FIBQuery,
  pFIBQuery, Variants, Controls, FIBDatabase, pFIBDatabase, pFIBStoredProc,
  frxClass, frxDBSet, cxCustomPivotGrid, cxTL, cxGridCardView,
  cxGridBandedTableView, cxStyles, cxGridTableView, cxClasses;

type
  Tdm = class(TDataModule)
    tblDogovori: TpFIBDataSet;
    dsDogovori: TDataSource;
    tblDogovoriID: TFIBIntegerField;
    tblDogovoriBROJ: TFIBStringField;
    tblDogovoriTIP_PARTNER: TFIBIntegerField;
    tblDogovoriPARTNER: TFIBIntegerField;
    tblDogovoriNAZIV: TFIBStringField;
    tblDogovoriNAZIV_SKRATEN: TFIBStringField;
    tblDogovoriOPIS: TFIBStringField;
    tblDogovoriCUSTOM1: TFIBStringField;
    tblDogovoriCUSTOM2: TFIBStringField;
    tblDogovoriCUSTOM3: TFIBStringField;
    tblDogovoriTS_INS: TFIBDateTimeField;
    tblDogovoriTS_UPD: TFIBDateTimeField;
    tblDogovoriUSR_INS: TFIBStringField;
    tblDogovoriUSR_UPD: TFIBStringField;
    tblDogovoriDATUM_OD: TFIBDateField;
    tblDogovoriDATUM_DO: TFIBDateField;
    dsDogovorniOrgani: TDataSource;
    tblDogovorniOrgani: TpFIBDataSet;
    tblDogovorniOrganiID: TFIBIntegerField;
    tblDogovorniOrganiDOGOVOR_ID: TFIBIntegerField;
    tblDogovorniOrganiTIP_PARTNER: TFIBIntegerField;
    tblDogovorniOrganiPARTNER: TFIBIntegerField;
    tblDogovorniOrganiNAZIV: TFIBStringField;
    tblDogovorniOrganiNAZIV_SKRATEN: TFIBStringField;
    tblDogovorniOrganiOPIS: TFIBStringField;
    tblDogovorniOrganiCUSTOM1: TFIBStringField;
    tblDogovorniOrganiCUSTOM2: TFIBStringField;
    tblDogovorniOrganiCUSTOM3: TFIBStringField;
    tblDogovorniOrganiTS_INS: TFIBDateTimeField;
    tblDogovorniOrganiTS_UPD: TFIBDateTimeField;
    tblDogovorniOrganiUSR_INS: TFIBStringField;
    tblDogovorniOrganiUSR_UPD: TFIBStringField;
    dslSANPaketi: TDataSource;
    tblSANPaketi: TpFIBDataSet;
    tblSANPaketiID: TFIBIntegerField;
    tblSANPaketiNAZIV: TFIBStringField;
    tblSANPaketiCENA: TFIBBCDField;
    tblSANPaketiTS_INS: TFIBDateTimeField;
    tblSANPaketiTS_UPD: TFIBDateTimeField;
    tblSANPaketiUSR_INS: TFIBStringField;
    tblSANPaketiUSR_UPD: TFIBStringField;
    tblSANPaketiKONTROLEN_PREGLED: TFIBSmallIntField;
    tblDogovoriSanPaketi: TpFIBDataSet;
    dsDogovoriSanPaketi: TDataSource;
    tblDogovoriSanPaketiID: TFIBIntegerField;
    tblDogovoriSanPaketiDOGOVOR_ID: TFIBIntegerField;
    tblDogovoriSanPaketiTS_INS: TFIBDateTimeField;
    tblDogovoriSanPaketiTS_UPD: TFIBDateTimeField;
    tblDogovoriSanPaketiUSR_INS: TFIBStringField;
    tblDogovoriSanPaketiUSR_UPD: TFIBStringField;
    tblDogovoriSanPaketiCUSTOM1: TFIBStringField;
    tblDogovoriSanPaketiCUSTOM2: TFIBStringField;
    tblDogovoriSanPaketiCUSTOM3: TFIBStringField;
    tblDogovoriSanPaketiPAKET: TFIBIntegerField;
    tblDogovoriSanPaketiPAKETNAZIV: TFIBStringField;
    qAktivenDog: TpFIBQuery;
    qAktivenDogovorDo: TpFIBQuery;
    tblMEDUslugi: TpFIBDataSet;
    dsMEDUslugi: TDataSource;
    tblMEDUslugiID: TFIBSmallIntField;
    tblMEDUslugiRE: TFIBIntegerField;
    tblMEDUslugiNAZIV: TFIBStringField;
    tblMEDUslugiBESPLATNA: TFIBStringField;
    tblMEDUslugiCENA_FZO: TFIBFloatField;
    tblMEDUslugiRE_NAPLATA: TFIBIntegerField;
    tblMEDUslugiSIFRA: TFIBStringField;
    tblMEDUslugiDEF_ANALIZA: TFIBSmallIntField;
    tblMEDUslugiPOSTAPKA: TFIBSmallIntField;
    tblMEDUslugiAKTIVNA: TFIBSmallIntField;
    tblMEDUslugiBR_ANALIZI: TFIBSmallIntField;
    tblMEDUslugiGRUPA: TFIBIntegerField;
    tblMEDUslugiKRATENKA: TFIBStringField;
    tblMEDUslugiPRIMEROK: TFIBIntegerField;
    tblMEDUslugiGRUPA_CENA: TFIBStringField;
    tblDogovoriMedUslugi: TpFIBDataSet;
    dsDogovoriMedUslugi: TDataSource;
    tblDogovoriMedUslugiID: TFIBIntegerField;
    tblDogovoriMedUslugiDOGOVOR_ID: TFIBIntegerField;
    tblDogovoriMedUslugiUSLUGA: TFIBIntegerField;
    tblDogovoriMedUslugiUSLUGA_NAZIV: TFIBStringField;
    tblDogovoriMedUslugiCENA: TFIBFloatField;
    tblDogovoriMedUslugiCUSTOM1: TFIBStringField;
    tblDogovoriMedUslugiCUSTOM2: TFIBStringField;
    tblDogovoriMedUslugiCUSTOM3: TFIBStringField;
    tblDogovoriMedUslugiTS_INS: TFIBDateTimeField;
    tblDogovoriMedUslugiTS_UPD: TFIBDateTimeField;
    tblDogovoriMedUslugiUSR_INS: TFIBStringField;
    tblDogovoriMedUslugiUSR_UPD: TFIBStringField;
    tblDogovoriMedUslugiUSLUGA_SIFRA: TFIBStringField;
    tblDogovoriSanPaketiCENA: TFIBFloatField;
    tblDogovoriCENA_IDENTIFIKACIJA: TFIBBCDField;
    tblDogovoriCENA_ANTIBIOGRAM: TFIBBCDField;
    tblValuta: TpFIBDataSet;
    dsValuta: TDataSource;
    tblValutaID: TFIBStringField;
    tblValutaNAZIV: TFIBStringField;
    tblValutaKURS: TFIBFloatField;
    qCountDogovorVoSan: TpFIBQuery;
    qCountDogovorVoMED: TpFIBQuery;
    tblDogovoriBROJ_DENOVI_FAKTURA: TFIBSmallIntField;
    tblSANUslugi: TpFIBDataSet;
    dsSANUslugi: TDataSource;
    tblSANUslugiID: TFIBIntegerField;
    tblSANUslugiNAZIV: TFIBStringField;
    tblSANUslugiCENA: TFIBBCDField;
    tblSANUslugiRE: TFIBIntegerField;
    tblSANUslugiSTATUS: TFIBSmallIntField;
    tblSANUslugiTIP: TFIBSmallIntField;
    tblSANUslugiTS_INS: TFIBDateTimeField;
    tblSANUslugiTS_UPD: TFIBDateTimeField;
    tblSANUslugiUSR_INS: TFIBStringField;
    tblSANUslugiUSR_UPD: TFIBStringField;
    tblSANUslugiRE_NAZIV: TFIBStringField;
    tblDogovoriSanUslugi: TpFIBDataSet;
    dsDogovoriSanUslugi: TDataSource;
    tblDogovoriSanUslugiID: TFIBIntegerField;
    tblDogovoriSanUslugiDOGOVOR_ID: TFIBIntegerField;
    tblDogovoriSanUslugiUSLUGA_ID: TFIBIntegerField;
    tblDogovoriSanUslugiUSLUGANAZIV: TFIBStringField;
    tblDogovoriSanUslugiTIP: TFIBSmallIntField;
    tblDogovoriSanUslugiCENA: TFIBFloatField;
    tblDogovoriSanUslugiTS_INS: TFIBDateTimeField;
    tblDogovoriSanUslugiTS_UPD: TFIBDateTimeField;
    tblDogovoriSanUslugiUSR_INS: TFIBStringField;
    tblDogovoriSanUslugiUSR_UPD: TFIBStringField;
    tblDogovoriSanUslugiCUSTOM1: TFIBStringField;
    tblDogovoriSanUslugiCUSTOM2: TFIBStringField;
    tblDogovoriSanUslugiCUSTOM3: TFIBStringField;
    qCountDogovorVoSanPaket: TpFIBQuery;
    qCountDogovorVoSanUslugi: TpFIBQuery;
    procedure TabelaBeforeDelete(DataSet: TDataSet);
    procedure insert6(Proc : TpFIBStoredProc; p1,  p2, p3, p4, p5,p6, v1, v2, v3, v4, v5, v6 : Variant);
    procedure tblDogovoriSanPaketiCENASetText(Sender: TField;
      const Text: string);
    procedure tblDogovoriMedUslugiCENASetText(Sender: TField;
      const Text: string);
    procedure tblDogovoriSanUslugiCENASetText(Sender: TField;
      const Text: string);
  private
    { Private declarations }
  public
    { Public declarations }

  end;

var
  dm: Tdm;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

uses dmKonekcija, DaNe, dmResources, Utils, Poraka;

{$R *.dfm}

procedure Tdm.TabelaBeforeDelete(DataSet: TDataSet);
begin
    frmDaNe := TfrmDaNe.Create(self, '������ �� �����', '���� ��������� ������ �� �� ��������� �������?', 1);
    if (frmDaNe.ShowModal <> mrYes) then
        Abort;
end;

procedure Tdm.tblDogovoriMedUslugiCENASetText(Sender: TField;
  const Text: string);
begin
     dm.qCountDogovorVoMED.Close;
     dm.qCountDogovorVoMED.ParamByName('dogovor_id').Value:=dm.tblDogovoriID.Value;
     dm.qCountDogovorVoMED.ExecQuery;
     if dm.qCountDogovorVoMED.FldByName['br'].Value = 0 then
        begin
          tblDogovoriMedUslugi.Edit;
          tblDogovoriMedUslugiCENA.Value:=StrToFloat(Text);
          tblDogovoriMedUslugi.Post;
        end
     else
        begin
          frmPoraka:=TfrmPoraka.Create(Self);
          frmPoraka.Tag:=1;
          frmPoraka.ShowModal;
          frmPoraka.Free;
        end;
end;

procedure Tdm.tblDogovoriSanPaketiCENASetText(Sender: TField;
  const Text: string);
begin
     dm.qCountDogovorVoSanPaket.Close;
     dm.qCountDogovorVoSanPaket.ParamByName('dogovor_id').Value:=dm.tblDogovoriID.Value;
     dm.qCountDogovorVoSanPaket.ParamByName('paket_id').Value:=dm.tblDogovoriSanPaketiPAKET.Value;
     dm.qCountDogovorVoSanPaket.ExecQuery;

     if dm.qCountDogovorVoSanPaket.FldByName['br'].Value = 0 then
       begin
          tblDogovoriSanPaketi.Edit;
          tblDogovoriSanPaketiCENA.Value:=StrToFloat(Text);
          tblDogovoriSanPaketi.Post;
       end
     else
        begin
          frmPoraka:=TfrmPoraka.Create(Self);
          frmPoraka.Tag:=3;
          frmPoraka.ShowModal;
          frmPoraka.Free;
        end;
end;

procedure Tdm.tblDogovoriSanUslugiCENASetText(Sender: TField;
  const Text: string);
begin
     dm.qCountDogovorVoSanUslugi.Close;
     dm.qCountDogovorVoSanUslugi.ParamByName('dogovor_id').Value:=dm.tblDogovoriID.Value;
     dm.qCountDogovorVoSanUslugi.ParamByName('usluga_id').Value:=dm.tblDogovoriSanUslugiUSLUGA_ID.Value;
     dm.qCountDogovorVoSanUslugi.ExecQuery;

     if dm.qCountDogovorVoSanUslugi.FldByName['br'].Value = 0 then
       begin
          tblDogovoriSanUslugi.Edit;
          tblDogovoriSanUslugiCENA.Value:=StrToFloat(Text);
          tblDogovoriSanUslugi.Post;
       end
     else
       begin
          frmPoraka:=TfrmPoraka.Create(Self);
          frmPoraka.Tag:=2;
          frmPoraka.ShowModal;
          frmPoraka.Free;
       end;
end;

procedure Tdm.insert6(Proc : TpFIBStoredProc; p1,  p2, p3, p4, p5,p6, v1, v2, v3, v4, v5, v6 : Variant);
    var
      ret : Extended;
    begin
         if(not VarIsNull(p1)) then
           Proc.ParamByName(VarToStr(p1)).Value := v1;
         if(not VarIsNull(p2)) then
           Proc.ParamByName(VarToStr(p2)).Value := v2;
         if(not VarIsNull(p3)) then
           Proc.ParamByName(VarToStr(p3)).Value := v3;
         if(not VarIsNull(p4)) then
           Proc.ParamByName(VarToStr(p4)).Value := v4;
         if(not VarIsNull(p5)) then
           Proc.ParamByName(VarToStr(p5)).Value := v5;
         if(not VarIsNull(p6)) then
           Proc.ParamByName(VarToStr(p6)).Value := v6;
         Proc.ExecProc;
    end;

end.
