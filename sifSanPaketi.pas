unit sifSanPaketi;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Master, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore, dxSkinBlack,
  dxSkinBlue, dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy,
  dxSkinGlassOceans, dxSkinHighContrast, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus, dxSkinSilver,
  dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008, dxSkinTheAsphaltWorld,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinVS2010, dxSkinWhiteprint,
  dxSkinXmas2008Blue, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, Data.DB, cxDBData, cxContainer, Vcl.Menus,
  dxRibbonSkins, dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, cxDropDownEdit,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSPDFExportCore,
  dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv, dxPSPrVwRibbon,
  dxPScxPageControlProducer, dxPScxGridLnk, dxPScxGridLayoutViewLnk,
  dxPScxEditorProducers, dxPScxExtEditorProducers, dxScreenTip, dxBar, dxPSCore,
  dxPScxCommon, Vcl.ActnList, cxBarEditItem, cxGridCustomPopupMenu,
  cxGridPopupMenu, dxStatusBar, dxRibbonStatusBar, cxClasses, dxRibbon,
  Vcl.StdCtrls, cxButtons, cxTextEdit, cxDBEdit, cxGridLevel, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  Vcl.ExtCtrls, dxSkinOffice2013White, cxNavigator, cxImageComboBox, cxCheckBox,
  System.Actions, dxSkinMetropolis, dxSkinMetropolisDark,
  dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray, dxSkinOffice2016Colorful,
  dxSkinOffice2016Dark, dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxRibbonCustomizationForm;

type
  TfrmSanPaketi = class(TfrmMaster)
    Label3: TLabel;
    NAZIV: TcxDBTextEdit;
    CENA: TcxDBTextEdit;
    Label4: TLabel;
    cxGrid1DBTableView1ID: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1CENA: TcxGridDBColumn;
    cxGrid1DBTableView1TS_INS: TcxGridDBColumn;
    cxGrid1DBTableView1TS_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1USR_INS: TcxGridDBColumn;
    cxGrid1DBTableView1USR_UPD: TcxGridDBColumn;
    dxBarLargeButton10: TdxBarLargeButton;
    aDefinicijaPaket: TAction;
    tipDefinicijaPaket: TdxScreenTip;
    cxCheckBoxKontrolen: TcxDBCheckBox;
    cxGrid1DBTableView1KONTROLEN_PREGLED: TcxGridDBColumn;
    dxBarLargeButton11: TdxBarLargeButton;
    aPrevzemiPaketi: TAction;
    procedure FormShow(Sender: TObject);
    procedure cxGrid1DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure cxGrid1DBTableView1DblClick(Sender: TObject);
    procedure prefrli;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmSanPaketi: TfrmSanPaketi;

implementation

{$R *.dfm}

uses dmUnit,  dmResources, DaNe;

procedure TfrmSanPaketi.cxGrid1DBTableView1DblClick(Sender: TObject);
begin
  inherited;
  prefrli;
end;

procedure TfrmSanPaketi.cxGrid1DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;
  if(Ord(Key) = VK_RETURN) then
    prefrli;
end;

procedure TfrmSanPaketi.FormClose(Sender: TObject; var Action: TCloseAction);
begin
//   if (cxGrid1DBTableView1.DataController.DataSource.State = dsEdit) or (cxGrid1DBTableView1.DataController.DataSource.State = dsInsert) then
//    begin
//        frmDaNe := TfrmDaNe.Create(self, '���������� ��������', '���������� �� �� ��������. ���� ������ �� �� �������?', 1);
//        if (frmDaNe.ShowModal <> mrYes) then
//        begin
//            cxGrid1DBTableView1.DataController.DataSet.Cancel;
//            Action := caFree;
//        end
//        else
//          if (Validacija(dPanel) = false) then
//          begin
//            cxGrid1DBTableView1.DataController.DataSet.Post;
//            Action := caFree;
//          end
//          else Action := caNone;
//    end;
end;

procedure TfrmSanPaketi.FormShow(Sender: TObject);
begin
  inherited;
  dm.tblSANPaketi.Close;
  dm.tblSANPaketi.Open;
end;

procedure TfrmSanPaketi.prefrli;
var flag, i:Integer;
begin
    inherited;
    if(not inserting) then
      begin
         ModalResult := mrOk;
         with cxGrid1DBTableView1.DataController do
         for I := 0 to cxGrid1DBTableView1.DataController.Controller.SelectedRecordCount -1 do  //izvrti gi site stiklirani - ne samo filtered
           begin
              flag:=cxGrid1DBTableView1.Controller.SelectedRecords[i].RecordIndex;
              dm.tblDogovoriSanPaketi.Insert;
              dm.tblDogovoriSanPaketiPAKET.Value:=GetValue(flag,cxGrid1DBTableView1ID.Index);
              dm.tblDogovoriSanPaketiCENA.Value:=GetValue(flag,cxGrid1DBTableView1CENA.Index);
              dm.tblDogovoriSanPaketiDOGOVOR_ID.Value:=dm.tblDogovoriID.Value;
              dm.tblDogovoriSanPaketi.Post;
            end;
      end;
end;

end.
