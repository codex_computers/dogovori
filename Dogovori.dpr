program Dogovori;

uses
  Vcl.Forms,
  dmKonekcija in '..\Share2010\dmKonekcija.pas' {dmKon: TDataModule},
  dmMaticni in '..\Share2010\dmMaticni.pas' {dmMat: TDataModule},
  dmResources in '..\Share2010\dmResources.pas' {dmRes: TDataModule},
  dmSystem in '..\Share2010\dmSystem.pas' {dmSys: TDataModule},
  Utils in '..\Share2010\Utils.pas',
  Login in '..\Share2010\Login.pas' {frmLogin},
  Master in '..\Share2010\Master.pas' {frmMaster},
  dmUnit in 'dmUnit.pas' {dm: TDataModule},
  Main in 'Main.pas' {frmMain},
  DaNe in '..\Share2010\DaNe.pas' {frmDaNe},
  DogovoriPoPaketi in 'DogovoriPoPaketi.pas' {frmDogovoriMedUslugi},
  Partner in '..\Share2010\Partner.pas' {frmPartner},
  sifMedUslugi in 'sifMedUslugi.pas' {frmMedUslugii},
  sifSanUslugi in 'sifSanUslugi.pas' {frmSanUslugi},
  sifSanPaketi in 'sifSanPaketi.pas' {frmSanPaketi},
  Poraka in 'Poraka.pas' {frmPoraka};

{$R *.res}

begin
  Application.Initialize;

  Application.Title := '�������� �� ������';
  Application.CreateForm(TdmMat, dmMat);
  Application.CreateForm(TdmRes, dmRes);
  Application.CreateForm(TdmKon, dmKon);
  Application.CreateForm(TdmSys, dmSys);
  dmKon.aplikacija:='DOG';


  if (dmKon.odbrana_baza and TfrmLogin.Execute) then
  begin
    Application.CreateForm(Tdm, dm);
    Application.CreateForm(TfrmMain, frmMain);
    Application.Run;
  end

  else
  begin
    dmMat.Free;
    dmRes.Free;
    dmKon.Free;
    dmSys.Free;
  end;
end.


