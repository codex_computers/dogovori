inherited frmSanPaketi: TfrmSanPaketi
  Caption = #1055#1072#1082#1077#1090#1080
  ClientWidth = 766
  ExplicitWidth = 782
  ExplicitHeight = 592
  PixelsPerInch = 96
  TextHeight = 13
  inherited lPanel: TPanel
    Width = 766
    ExplicitWidth = 766
    inherited cxGrid1: TcxGrid
      Width = 762
      ExplicitWidth = 762
      inherited cxGrid1DBTableView1: TcxGridDBTableView
        OnDblClick = cxGrid1DBTableView1DblClick
        DataController.DataSource = dm.dslSANPaketi
        OptionsSelection.MultiSelect = True
        object cxGrid1DBTableView1ID: TcxGridDBColumn
          DataBinding.FieldName = 'ID'
          Options.Editing = False
        end
        object cxGrid1DBTableView1NAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'NAZIV'
          Options.Editing = False
          Width = 487
        end
        object cxGrid1DBTableView1CENA: TcxGridDBColumn
          DataBinding.FieldName = 'CENA'
          Options.Editing = False
          Width = 72
        end
        object cxGrid1DBTableView1KONTROLEN_PREGLED: TcxGridDBColumn
          DataBinding.FieldName = 'KONTROLEN_PREGLED'
          PropertiesClassName = 'TcxImageComboBoxProperties'
          Properties.Images = dmRes.cxSmallImages
          Properties.Items = <
            item
              Description = #1050#1086#1085#1090#1086#1083#1077#1085' '#1087#1088#1077#1075#1083#1077#1076
              ImageIndex = 6
              Value = 1
            end>
          Options.Editing = False
          Width = 116
        end
        object cxGrid1DBTableView1TS_INS: TcxGridDBColumn
          DataBinding.FieldName = 'TS_INS'
          Visible = False
          Options.Editing = False
          Width = 166
        end
        object cxGrid1DBTableView1TS_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'TS_UPD'
          Visible = False
          Options.Editing = False
          Width = 215
        end
        object cxGrid1DBTableView1USR_INS: TcxGridDBColumn
          DataBinding.FieldName = 'USR_INS'
          Visible = False
          Options.Editing = False
          Width = 207
        end
        object cxGrid1DBTableView1USR_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'USR_UPD'
          Visible = False
          Options.Editing = False
          Width = 236
        end
      end
    end
  end
  inherited dPanel: TPanel
    Width = 766
    ExplicitWidth = 766
    inherited Label1: TLabel
      Left = 17
      Top = 22
      ExplicitLeft = 17
      ExplicitTop = 22
    end
    object Label3: TLabel [1]
      Left = 13
      Top = 49
      Width = 50
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1053#1072#1079#1080#1074' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label4: TLabel [2]
      Left = 13
      Top = 76
      Width = 50
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1062#1077#1085#1072' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    inherited Sifra: TcxDBTextEdit
      Left = 69
      Top = 19
      Hint = #1064#1080#1092#1088#1072' '#1085#1072' '#1087#1072#1082#1077#1090
      DataBinding.DataField = 'ID'
      DataBinding.DataSource = dm.dslSANPaketi
      ExplicitLeft = 69
      ExplicitTop = 19
    end
    inherited OtkaziButton: TcxButton
      Left = 675
      TabOrder = 5
      Visible = False
      ExplicitLeft = 675
    end
    inherited ZapisiButton: TcxButton
      Left = 594
      TabOrder = 4
      Visible = False
      ExplicitLeft = 594
    end
    object NAZIV: TcxDBTextEdit
      Tag = 1
      Left = 69
      Top = 46
      Hint = #1053#1072#1079#1080#1074' '#1085#1072' '#1087#1072#1082#1077#1090
      BeepOnEnter = False
      DataBinding.DataField = 'NAZIV'
      DataBinding.DataSource = dm.dslSANPaketi
      ParentFont = False
      Properties.BeepOnError = True
      Style.Shadow = False
      TabOrder = 1
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 476
    end
    object CENA: TcxDBTextEdit
      Tag = 1
      Left = 69
      Top = 73
      Hint = #1062#1077#1085#1072' '#1085#1072' '#1087#1072#1082#1077#1090
      BeepOnEnter = False
      DataBinding.DataField = 'CENA'
      DataBinding.DataSource = dm.dslSANPaketi
      ParentFont = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      Style.Shadow = False
      TabOrder = 2
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 80
    end
    object cxCheckBoxKontrolen: TcxDBCheckBox
      Left = 70
      Top = 100
      TabStop = False
      Caption = #1050#1086#1085#1090#1088#1086#1083#1077#1085' '#1087#1088#1077#1075#1083#1077#1076
      DataBinding.DataField = 'KONTROLEN_PREGLED'
      Properties.Alignment = taRightJustify
      Properties.ValueChecked = 1
      Properties.ValueUnchecked = 0
      TabOrder = 3
      Transparent = True
      Width = 124
    end
  end
  inherited dxRibbon1: TdxRibbon
    Width = 766
    ExplicitWidth = 766
    inherited dxRibbon1Tab1: TdxRibbonTab
      Index = 0
    end
    inherited dxRibbon1Tab2: TdxRibbonTab
      Index = 1
    end
  end
  inherited StatusBar1: TdxRibbonStatusBar
    Width = 766
    Panels = <
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = 'Enter '#1080#1083#1080' '#1044#1074#1086#1077#1085' '#1082#1083#1080#1082' - '#1047#1072#1087#1080#1096#1080', Esc - '#1054#1090#1082#1072#1078#1080' / '#1048#1079#1083#1077#1079
      end>
    ExplicitTop = 32000
    ExplicitWidth = 766
  end
  inherited PopupMenu1: TPopupMenu
    Top = 216
  end
  inherited dxBarManager1: TdxBarManager
    Left = 448
    Top = 240
    PixelsPerInch = 96
    inherited dxBarManager1Bar1: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 69
      FloatClientHeight = 167
      Visible = False
    end
    inherited dxBarManager1Bar2: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      DockedLeft = 0
      FloatClientWidth = 149
      FloatClientHeight = 189
    end
    inherited dxBarManager1Bar3: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      DockedLeft = 320
      FloatClientWidth = 59
      FloatClientHeight = 108
    end
    inherited dxBarManager1Bar4: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 141
      FloatClientHeight = 221
    end
    inherited dxBarManager1BarTabela: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 174
      FloatClientHeight = 113
    end
    object dxBarLargeButton10: TdxBarLargeButton
      Action = aDefinicijaPaket
      Category = 0
      ScreenTip = tipDefinicijaPaket
    end
    object dxBarLargeButton11: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
  end
  inherited ActionList1: TActionList
    object aDefinicijaPaket: TAction
      Caption = #1044#1077#1092#1080#1085#1080#1094#1080#1112#1072' '#1085#1072' '#1087#1072#1082#1077#1090
      ImageIndex = 66
    end
    object aPrevzemiPaketi: TAction
      Caption = #1055#1088#1077#1074#1079#1077#1084#1080' '#1075#1080' '#1089#1077#1083#1077#1082#1090#1080#1088#1072#1085#1080#1090#1077' '#1087#1072#1082#1077#1090#1080
      ImageIndex = 62
    end
  end
  inherited dxComponentPrinter1: TdxComponentPrinter
    inherited dxComponentPrinter1Link1: TdxGridReportLink
      ReportDocument.CreationDate = 41330.419730601850000000
      Styles.BandHeader = nil
      Styles.Caption = nil
      Styles.CardCaptionRow = nil
      Styles.CardRowCaption = nil
      Styles.Content = nil
      Styles.ContentEven = nil
      Styles.ContentOdd = nil
      Styles.FilterBar = nil
      Styles.Footer = nil
      Styles.Group = nil
      Styles.Header = nil
      Styles.Preview = nil
      Styles.Selection = nil
      BuiltInReportLink = True
    end
  end
  inherited dxBarScreenTipRepository1: TdxBarScreenTipRepository
    Left = 696
    Top = 208
    PixelsPerInch = 96
    object tipDefinicijaPaket: TdxScreenTip
      Header.Text = #1044#1077#1092#1080#1085#1080#1094#1080#1112#1072' '#1085#1072' '#1087#1072#1082#1077#1090
      Description.Text = #1044#1077#1092#1080#1085#1080#1088#1072#1114#1077' '#1085#1072' '#1091#1089#1091#1075#1080#1090#1077' '#1082#1086#1080' '#1089#1087#1072#1107#1072#1072#1090' '#1074#1086' '#1089#1077#1083#1077#1082#1090#1080#1088#1072#1085#1080#1086#1090' '#1087#1072#1082#1077#1090#13#10
    end
  end
end
