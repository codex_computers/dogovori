object frmDogovoriMedUslugi: TfrmDogovoriMedUslugi
  Left = 128
  Top = 212
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = #1044#1086#1075#1086#1074#1086#1088#1080' '#1079#1072' '#1084#1077#1076#1080#1094#1080#1085#1089#1082#1080' '#1091#1089#1083#1091#1075#1080
  ClientHeight = 729
  ClientWidth = 1115
  Color = clBtnFace
  Constraints.MinHeight = 500
  Constraints.MinWidth = 610
  Font.Charset = RUSSIAN_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object dxRibbon1: TdxRibbon
    Left = 0
    Top = 0
    Width = 1115
    Height = 126
    BarManager = dxBarManager1
    ColorSchemeName = 'Blue'
    Contexts = <>
    TabOrder = 0
    TabStop = False
    object dxRibbon1Tab1: TdxRibbonTab
      Active = True
      Caption = #1052#1077#1085#1080
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar1'
        end
        item
          ToolbarName = 'dxBarManager1Bar2'
        end
        item
          ToolbarName = 'dxBarManager1Bar3'
        end>
      Index = 0
    end
    object dxRibbon1Tab2: TdxRibbonTab
      Caption = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar4'
        end
        item
          ToolbarName = 'dxBarManager1BarIzgledGrid'
        end>
      Index = 1
    end
  end
  object StatusBar1: TdxRibbonStatusBar
    Left = 0
    Top = 706
    Width = 1115
    Height = 23
    Panels = <
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = 
          ' F5 - '#1053#1086#1074', F6 - '#1040#1078#1091#1088#1080#1088#1072#1112', F7 - '#1054#1089#1074#1077#1078#1080', F8 - '#1041#1088#1080#1096#1080', F9 - '#1047#1072#1087#1080#1096#1080', ' +
          'Esc - '#1054#1090#1082#1072#1078#1080' / '#1048#1079#1083#1077#1079
      end>
    Ribbon = dxRibbon1
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clDefault
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
  end
  object Panel1: TPanel
    Left = 0
    Top = 126
    Width = 1115
    Height = 259
    Align = alTop
    TabOrder = 2
    object PanelDogovor: TPanel
      Left = 576
      Top = 1
      Width = 538
      Height = 257
      Align = alRight
      Enabled = False
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      DesignSize = (
        538
        257)
      object Label14: TLabel
        Left = 32
        Top = 10
        Width = 114
        Height = 13
        AutoSize = False
        Caption = #1044#1086#1075#1086#1074#1086#1088#1077#1085' '#1086#1088#1075#1072#1085' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label6: TLabel
        Left = 32
        Top = 49
        Width = 39
        Height = 13
        AutoSize = False
        Caption = #1041#1088#1086#1112' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label1: TLabel
        Left = 119
        Top = 49
        Width = 138
        Height = 13
        AutoSize = False
        Caption = #1055#1088#1077#1076#1084#1077#1090' '#1085#1072' '#1076#1086#1075#1086#1074#1086#1088' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object PARTNERNAZIV: TcxExtLookupComboBox
        Left = 106
        Top = 25
        Hint = #1044#1086#1075#1086#1074#1086#1088#1077#1085' '#1086#1088#1075#1072#1085' - '#1085#1072#1079#1080#1074
        Anchors = [akLeft, akTop, akRight]
        Properties.ClearKey = 46
        Properties.DropDownSizeable = True
        Properties.OnCloseUp = PARTNERNAZIVPropertiesCloseUp
        TabOrder = 2
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 407
      end
      object PARTNER: TcxDBTextEdit
        Tag = 1
        Left = 67
        Top = 25
        Hint = #1044#1086#1075#1086#1074#1086#1088#1077#1085' '#1086#1088#1075#1072#1085' - '#1096#1080#1092#1088#1072
        BeepOnEnter = False
        DataBinding.DataField = 'PARTNER'
        DataBinding.DataSource = dm.dsDogovori
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Style.Shadow = False
        TabOrder = 1
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 40
      end
      object TIP_PARTNER: TcxDBTextEdit
        Tag = 1
        Left = 32
        Top = 25
        Hint = #1044#1086#1075#1086#1074#1086#1088#1077#1085' '#1086#1088#1075#1072#1085' - '#1090#1080#1087
        BeepOnEnter = False
        DataBinding.DataField = 'TIP_PARTNER'
        DataBinding.DataSource = dm.dsDogovori
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Style.Shadow = False
        TabOrder = 0
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 35
      end
      object BROJ: TcxDBTextEdit
        Tag = 1
        Left = 32
        Top = 64
        Hint = #1041#1088#1086#1112' '#1085#1072' '#1076#1086#1075#1086#1074#1086#1088
        BeepOnEnter = False
        DataBinding.DataField = 'BROJ'
        DataBinding.DataSource = dm.dsDogovori
        ParentFont = False
        Properties.BeepOnError = True
        Style.Shadow = False
        TabOrder = 3
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 81
      end
      object cxGroupBox1: TcxGroupBox
        Left = 33
        Top = 91
        TabStop = True
        Caption = #1055#1077#1088#1080#1086#1076' '#1085#1072' '#1074#1072#1078#1077#1114#1077' '#1085#1072' '#1076#1086#1075#1086#1074#1086#1088
        TabOrder = 5
        Height = 70
        Width = 258
        object Label11: TLabel
          Left = 19
          Top = 19
          Width = 71
          Height = 13
          AutoSize = False
          Caption = #1044#1072#1090#1091#1084' '#1086#1076' :'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label12: TLabel
          Left = 144
          Top = 19
          Width = 70
          Height = 13
          AutoSize = False
          Caption = #1044#1072#1090#1091#1084' '#1076#1086' :'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object DATUM_DO: TcxDBDateEdit
          Tag = 1
          Left = 140
          Top = 34
          Hint = #1044#1072#1090#1091#1084' '#1082#1086#1075#1072' '#1087#1086#1074#1090#1086#1088#1085#1086' '#1090#1088#1077#1073#1072' '#1076#1072' '#1087#1088#1072#1074#1080' '#1089#1072#1085#1080#1090#1072#1088#1077#1085' '#1087#1088#1077#1075#1083#1077#1076
          DataBinding.DataField = 'DATUM_DO'
          DataBinding.DataSource = dm.dsDogovori
          TabOrder = 1
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Width = 102
        end
        object DATUM_OD: TcxDBDateEdit
          Tag = 1
          Left = 19
          Top = 34
          DataBinding.DataField = 'DATUM_OD'
          DataBinding.DataSource = dm.dsDogovori
          TabOrder = 0
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Width = 102
        end
      end
      object cxButton8: TcxButton
        Left = 355
        Top = 207
        Width = 75
        Height = 25
        Action = aZapisi
        OptionsImage.Images = dmRes.cxSmallImages
        TabOrder = 7
      end
      object cxButton9: TcxButton
        Left = 436
        Top = 207
        Width = 75
        Height = 25
        Action = aOtkazi
        OptionsImage.Images = dmRes.cxSmallImages
        TabOrder = 8
      end
      object OPIS: TcxDBMemo
        Left = 119
        Top = 64
        DataBinding.DataField = 'OPIS'
        DataBinding.DataSource = dm.dsDogovori
        TabOrder = 4
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Height = 21
        Width = 394
      end
      object cxGroupBox2: TcxGroupBox
        Left = 33
        Top = 167
        Caption = #1055#1086#1076#1072#1090#1086#1094#1080' '#1079#1072' '#1092#1072#1082#1090#1091#1088#1072
        TabOrder = 6
        Height = 70
        Width = 258
        object Label4: TLabel
          Left = 16
          Top = 19
          Width = 185
          Height = 13
          AutoSize = False
          Caption = #1041#1088#1086#1112' '#1085#1072' '#1076#1077#1085#1086#1074#1080' '#1079#1072' '#1076#1086#1089#1087#1077#1074#1072#1114#1077' :'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object BROJ_DENOVI_FAKTURA: TcxDBTextEdit
          Tag = 1
          Left = 13
          Top = 34
          Hint = #1041#1088#1086#1112' '#1085#1072' '#1076#1086#1075#1086#1074#1086#1088
          BeepOnEnter = False
          DataBinding.DataField = 'BROJ_DENOVI_FAKTURA'
          DataBinding.DataSource = dm.dsDogovori
          ParentFont = False
          Properties.BeepOnError = True
          Style.Shadow = False
          TabOrder = 0
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Width = 228
        end
      end
    end
    object Panel3: TPanel
      Left = 1
      Top = 1
      Width = 575
      Height = 257
      Align = alClient
      Caption = 'Panel3'
      TabOrder = 1
      object cxGrid1: TcxGrid
        Left = 1
        Top = 1
        Width = 573
        Height = 255
        Align = alClient
        TabOrder = 0
        object cxGrid1DBTableView1: TcxGridDBTableView
          OnKeyPress = cxGrid1DBTableView1KeyPress
          Navigator.Buttons.CustomButtons = <>
          OnFocusedRecordChanged = cxGrid1DBTableView1FocusedRecordChanged
          DataController.DataSource = dm.dsDogovori
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          FilterRow.Visible = True
          OptionsBehavior.IncSearch = True
          OptionsCustomize.ColumnsQuickCustomization = True
          OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
          OptionsData.Deleting = False
          OptionsData.Editing = False
          OptionsData.Inserting = False
          object cxGrid1DBTableView1ID: TcxGridDBColumn
            DataBinding.FieldName = 'ID'
            Visible = False
            Width = 48
          end
          object cxGrid1DBTableView1BROJ: TcxGridDBColumn
            DataBinding.FieldName = 'BROJ'
            Width = 66
          end
          object cxGrid1DBTableView1TIP_PARTNER: TcxGridDBColumn
            DataBinding.FieldName = 'TIP_PARTNER'
            Visible = False
            Width = 123
          end
          object cxGrid1DBTableView1PARTNER: TcxGridDBColumn
            DataBinding.FieldName = 'PARTNER'
            Width = 141
          end
          object cxGrid1DBTableView1NAZIV: TcxGridDBColumn
            DataBinding.FieldName = 'NAZIV'
            Width = 247
          end
          object cxGrid1DBTableView1NAZIV_SKRATEN: TcxGridDBColumn
            DataBinding.FieldName = 'NAZIV_SKRATEN'
            Visible = False
            Width = 180
          end
          object cxGrid1DBTableView1DATUM_OD: TcxGridDBColumn
            DataBinding.FieldName = 'DATUM_OD'
            Width = 74
          end
          object cxGrid1DBTableView1DATUM_DO: TcxGridDBColumn
            DataBinding.FieldName = 'DATUM_DO'
            Width = 68
          end
          object cxGrid1DBTableView1OPIS: TcxGridDBColumn
            DataBinding.FieldName = 'OPIS'
            Width = 176
          end
          object cxGrid1DBTableView1CENA_IDENTIFIKACIJA: TcxGridDBColumn
            DataBinding.FieldName = 'CENA_IDENTIFIKACIJA'
            Width = 226
          end
          object cxGrid1DBTableView1CENA_ANTIBIOGRAM: TcxGridDBColumn
            DataBinding.FieldName = 'CENA_ANTIBIOGRAM'
            Width = 133
          end
          object cxGrid1DBTableView1BROJ_DENOVI_FAKTURA: TcxGridDBColumn
            DataBinding.FieldName = 'BROJ_DENOVI_FAKTURA'
            Width = 165
          end
          object cxGrid1DBTableView1CUSTOM1: TcxGridDBColumn
            DataBinding.FieldName = 'CUSTOM1'
            Visible = False
            Width = 100
          end
          object cxGrid1DBTableView1CUSTOM2: TcxGridDBColumn
            DataBinding.FieldName = 'CUSTOM2'
            Visible = False
            Width = 100
          end
          object cxGrid1DBTableView1CUSTOM3: TcxGridDBColumn
            DataBinding.FieldName = 'CUSTOM3'
            Visible = False
            Width = 100
          end
          object cxGrid1DBTableView1TS_INS: TcxGridDBColumn
            DataBinding.FieldName = 'TS_INS'
            Visible = False
            Width = 161
          end
          object cxGrid1DBTableView1TS_UPD: TcxGridDBColumn
            DataBinding.FieldName = 'TS_UPD'
            Visible = False
            Width = 212
          end
          object cxGrid1DBTableView1USR_INS: TcxGridDBColumn
            DataBinding.FieldName = 'USR_INS'
            Visible = False
            Width = 203
          end
          object cxGrid1DBTableView1USR_UPD: TcxGridDBColumn
            DataBinding.FieldName = 'USR_UPD'
            Visible = False
            Width = 245
          end
        end
        object cxGrid1Level1: TcxGridLevel
          GridView = cxGrid1DBTableView1
        end
      end
    end
  end
  object cxButton1: TcxButton
    Left = 184
    Top = 628
    Width = 89
    Height = 25
    Action = aDodadiDogovorenOrgan
    TabOrder = 3
  end
  object cxPageControl1: TcxPageControl
    Left = 0
    Top = 385
    Width = 1115
    Height = 321
    Align = alClient
    TabOrder = 4
    Properties.ActivePage = cxTabSheetSANUslugi
    Properties.CustomButtons.Buttons = <>
    Properties.Images = dmRes.cxLargeImages
    Properties.Rotate = True
    Properties.TabHeight = 40
    Properties.TabPosition = tpLeft
    ClientRectBottom = 321
    ClientRectLeft = 153
    ClientRectRight = 1115
    ClientRectTop = 0
    object cxTabSheetPartneri: TcxTabSheet
      Caption = #1044#1086#1075#1086#1074#1086#1088#1077#1085' '#1086#1088#1075#1072#1085
      ImageIndex = 55
      object pnl1: TPanel
        Left = 0
        Top = 278
        Width = 962
        Height = 43
        Align = alBottom
        TabOrder = 0
        object btnaDodadiDogovorenOrgan: TcxButton
          Left = 19
          Top = 10
          Width = 75
          Height = 25
          Action = aDodadiDogovorenOrgan
          OptionsImage.Images = dmRes.cxSmallImages
          TabOrder = 0
        end
        object btnaIzvadiDogovorenOrgan: TcxButton
          Left = 100
          Top = 10
          Width = 75
          Height = 25
          Action = aIzvadiDogovorenOrgan
          OptionsImage.Images = dmRes.cxSmallImages
          TabOrder = 1
        end
      end
      object cxgrid5: TcxGrid
        Left = 0
        Top = 0
        Width = 962
        Height = 278
        Align = alClient
        TabOrder = 1
        object cxGrid2DBTableView1: TcxGridDBTableView
          OnKeyPress = cxGrid2DBTableView1KeyPress
          Navigator.Buttons.CustomButtons = <>
          DataController.DataSource = dm.dsDogovorniOrgani
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          FilterRow.Visible = True
          OptionsBehavior.IncSearch = True
          OptionsCustomize.ColumnsQuickCustomization = True
          OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
          OptionsData.Deleting = False
          OptionsData.Editing = False
          OptionsData.Inserting = False
          OptionsView.GroupByBox = False
          object cxGrid2DBTableView1ID: TcxGridDBColumn
            DataBinding.FieldName = 'ID'
            Visible = False
            Width = 58
          end
          object cxGrid2DBTableView1DOGOVOR_ID: TcxGridDBColumn
            DataBinding.FieldName = 'DOGOVOR_ID'
            Visible = False
            Width = 115
          end
          object cxGrid2DBTableView1TIP_PARTNER: TcxGridDBColumn
            DataBinding.FieldName = 'TIP_PARTNER'
            Visible = False
            Width = 124
          end
          object cxGrid2DBTableView1PARTNER: TcxGridDBColumn
            DataBinding.FieldName = 'PARTNER'
            Width = 138
          end
          object cxGrid2DBTableView1NAZIV: TcxGridDBColumn
            DataBinding.FieldName = 'NAZIV'
            Width = 664
          end
          object cxGrid2DBTableView1NAZIV_SKRATEN: TcxGridDBColumn
            DataBinding.FieldName = 'NAZIV_SKRATEN'
            Visible = False
            Width = 250
          end
          object cxGrid2DBTableView1OPIS: TcxGridDBColumn
            DataBinding.FieldName = 'OPIS'
            Visible = False
            Width = 250
          end
          object cxGrid2DBTableView1CUSTOM1: TcxGridDBColumn
            DataBinding.FieldName = 'CUSTOM1'
            Visible = False
            Width = 250
          end
          object cxGrid2DBTableView1CUSTOM2: TcxGridDBColumn
            DataBinding.FieldName = 'CUSTOM2'
            Visible = False
            Width = 250
          end
          object cxGrid2DBTableView1CUSTOM3: TcxGridDBColumn
            DataBinding.FieldName = 'CUSTOM3'
            Visible = False
            Width = 250
          end
          object cxGrid2DBTableView1TS_INS: TcxGridDBColumn
            DataBinding.FieldName = 'TS_INS'
            Visible = False
            Width = 250
          end
          object cxGrid2DBTableView1TS_UPD: TcxGridDBColumn
            DataBinding.FieldName = 'TS_UPD'
            Visible = False
            Width = 250
          end
          object cxGrid2DBTableView1USR_INS: TcxGridDBColumn
            DataBinding.FieldName = 'USR_INS'
            Visible = False
            Width = 250
          end
          object cxGrid2DBTableView1USR_UPD: TcxGridDBColumn
            DataBinding.FieldName = 'USR_UPD'
            Visible = False
            Width = 250
          end
        end
        object cxGrid1Grid2Level1: TcxGridLevel
          GridView = cxGrid2DBTableView1
        end
      end
    end
    object cxTabSheetSANPaketi: TcxTabSheet
      Caption = #1057#1072#1085#1080#1090#1072#1088#1085#1080' - '#1055#1072#1082#1077#1090#1080
      ImageIndex = 40
      object pnl2: TPanel
        Left = 0
        Top = 278
        Width = 962
        Height = 43
        Align = alBottom
        TabOrder = 0
        object btnaIzvadiPAket: TcxButton
          Left = 100
          Top = 10
          Width = 75
          Height = 25
          Action = aIzvadiPAket
          OptionsImage.Images = dmRes.cxSmallImages
          TabOrder = 0
        end
        object btnaDodadiPaket: TcxButton
          Left = 19
          Top = 10
          Width = 75
          Height = 25
          Action = aDodadiPaket
          OptionsImage.Images = dmRes.cxSmallImages
          TabOrder = 1
        end
      end
      object cxgrid2: TcxGrid
        Left = 0
        Top = 0
        Width = 962
        Height = 278
        Align = alClient
        TabOrder = 1
        object cxGrid3DBTableView1: TcxGridDBTableView
          OnKeyPress = cxGrid3DBTableView1KeyPress
          Navigator.Buttons.CustomButtons = <>
          DataController.DataSource = dm.dsDogovoriSanPaketi
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <
            item
              Format = '0.00,.'
              Kind = skSum
              Column = cxGrid3DBTableView1CENA
            end>
          DataController.Summary.SummaryGroups = <>
          FilterRow.Visible = True
          OptionsBehavior.IncSearch = True
          OptionsCustomize.ColumnsQuickCustomization = True
          OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
          OptionsData.Deleting = False
          OptionsData.Inserting = False
          OptionsView.Footer = True
          OptionsView.GroupByBox = False
          object cxGrid3DBTableView1ID: TcxGridDBColumn
            DataBinding.FieldName = 'ID'
            Visible = False
            Options.Editing = False
          end
          object cxGrid3DBTableView1DOGOVOR_ID: TcxGridDBColumn
            DataBinding.FieldName = 'DOGOVOR_ID'
            Visible = False
            Options.Editing = False
            Width = 106
          end
          object cxGrid3DBTableView1PAKET: TcxGridDBColumn
            DataBinding.FieldName = 'PAKET'
            Visible = False
            Options.Editing = False
          end
          object cxGrid3DBTableView1PAKETNAZIV: TcxGridDBColumn
            DataBinding.FieldName = 'PAKETNAZIV'
            Options.Editing = False
            Width = 482
          end
          object cxGrid3DBTableView1CENA: TcxGridDBColumn
            DataBinding.FieldName = 'CENA'
            Styles.Content = dmRes.cxStyle37
            Width = 109
          end
          object cxGrid3DBTableView1TS_INS: TcxGridDBColumn
            DataBinding.FieldName = 'TS_INS'
            Visible = False
            Options.Editing = False
            Width = 250
          end
          object cxGrid3DBTableView1TS_UPD: TcxGridDBColumn
            DataBinding.FieldName = 'TS_UPD'
            Visible = False
            Options.Editing = False
            Width = 250
          end
          object cxGrid3DBTableView1USR_INS: TcxGridDBColumn
            DataBinding.FieldName = 'USR_INS'
            Visible = False
            Options.Editing = False
            Width = 250
          end
          object cxGrid3DBTableView1USR_UPD: TcxGridDBColumn
            DataBinding.FieldName = 'USR_UPD'
            Visible = False
            Options.Editing = False
            Width = 250
          end
          object cxGrid3DBTableView1CUSTOM1: TcxGridDBColumn
            DataBinding.FieldName = 'CUSTOM1'
            Visible = False
            Options.Editing = False
            Width = 250
          end
          object cxGrid3DBTableView1CUSTOM2: TcxGridDBColumn
            DataBinding.FieldName = 'CUSTOM2'
            Visible = False
            Options.Editing = False
            Width = 250
          end
          object cxGrid3DBTableView1CUSTOM3: TcxGridDBColumn
            DataBinding.FieldName = 'CUSTOM3'
            Visible = False
            Options.Editing = False
            Width = 250
          end
        end
        object cxGrid1Grid3Level1: TcxGridLevel
          GridView = cxGrid3DBTableView1
        end
      end
    end
    object cxTabSheetSANUslugi: TcxTabSheet
      Caption = #1057#1072#1085#1080#1090#1072#1088#1085#1080' - '#1059#1089#1083#1091#1075#1080
      ImageIndex = 40
      object cxGrid4: TcxGrid
        Left = 0
        Top = 0
        Width = 962
        Height = 278
        Align = alClient
        TabOrder = 0
        object cxGrid1DBTableView2: TcxGridDBTableView
          OnKeyPress = cxGrid1DBTableView2KeyPress
          Navigator.Buttons.CustomButtons = <>
          DataController.DataSource = dm.dsDogovoriSanUslugi
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <
            item
              Format = '0.00,.'
              Kind = skSum
            end
            item
              Format = '0.00,.'
              Kind = skSum
              Column = cxGrid1DBTableView2CENA
            end>
          DataController.Summary.SummaryGroups = <>
          FilterRow.Visible = True
          OptionsBehavior.IncSearch = True
          OptionsCustomize.ColumnsQuickCustomization = True
          OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
          OptionsData.Deleting = False
          OptionsData.Inserting = False
          OptionsView.Footer = True
          OptionsView.GroupByBox = False
          object cxGrid1DBTableView2ID: TcxGridDBColumn
            DataBinding.FieldName = 'ID'
            Visible = False
            Options.Editing = False
            Width = 57
          end
          object cxGrid1DBTableView2DOGOVOR_ID: TcxGridDBColumn
            DataBinding.FieldName = 'DOGOVOR_ID'
            Visible = False
            Options.Editing = False
            Width = 109
          end
          object cxGrid1DBTableView2USLUGA_ID: TcxGridDBColumn
            DataBinding.FieldName = 'USLUGA_ID'
            Visible = False
            Options.Editing = False
            Width = 87
          end
          object cxGrid1DBTableView2USLUGANAZIV: TcxGridDBColumn
            DataBinding.FieldName = 'USLUGANAZIV'
            Options.Editing = False
            Width = 440
          end
          object cxGrid1DBTableView2TIP: TcxGridDBColumn
            DataBinding.FieldName = 'TIP'
            PropertiesClassName = 'TcxImageComboBoxProperties'
            Properties.Images = dmRes.cxImageGrid
            Properties.Items = <
              item
                Description = #1059#1089#1083#1091#1075#1072
                ImageIndex = 2
                Value = 0
              end
              item
                Description = #1052#1072#1085#1080#1087#1091#1083#1072#1090#1080#1074#1077#1085' '#1090#1088#1086#1096#1086#1082
                ImageIndex = 4
                Value = 1
              end>
            Options.Editing = False
            Width = 180
          end
          object cxGrid1DBTableView2CENA: TcxGridDBColumn
            DataBinding.FieldName = 'CENA'
            Styles.Content = dmRes.cxStyle37
            Width = 81
          end
          object cxGrid1DBTableView2TS_INS: TcxGridDBColumn
            DataBinding.FieldName = 'TS_INS'
            Visible = False
            Options.Editing = False
            Width = 169
          end
          object cxGrid1DBTableView2TS_UPD: TcxGridDBColumn
            DataBinding.FieldName = 'TS_UPD'
            Visible = False
            Options.Editing = False
            Width = 218
          end
          object cxGrid1DBTableView2USR_INS: TcxGridDBColumn
            DataBinding.FieldName = 'USR_INS'
            Visible = False
            Options.Editing = False
            Width = 210
          end
          object cxGrid1DBTableView2USR_UPD: TcxGridDBColumn
            DataBinding.FieldName = 'USR_UPD'
            Visible = False
            Options.Editing = False
            Width = 236
          end
          object cxGrid1DBTableView2CUSTOM1: TcxGridDBColumn
            DataBinding.FieldName = 'CUSTOM1'
            Visible = False
            Options.Editing = False
            Width = 113
          end
          object cxGrid1DBTableView2CUSTOM2: TcxGridDBColumn
            DataBinding.FieldName = 'CUSTOM2'
            Visible = False
            Options.Editing = False
            Width = 134
          end
          object cxGrid1DBTableView2CUSTOM3: TcxGridDBColumn
            DataBinding.FieldName = 'CUSTOM3'
            Visible = False
            Options.Editing = False
            Width = 113
          end
        end
        object cxGridLevel1: TcxGridLevel
          GridView = cxGrid1DBTableView2
        end
      end
      object pnl5: TPanel
        Left = 0
        Top = 278
        Width = 962
        Height = 43
        Align = alBottom
        TabOrder = 1
        object btnaIzvadiPAket1: TcxButton
          Left = 100
          Top = 10
          Width = 75
          Height = 25
          Action = aIzvadiSanUslugi
          OptionsImage.Images = dmRes.cxSmallImages
          TabOrder = 0
        end
        object btnaDodadiPaket1: TcxButton
          Left = 19
          Top = 10
          Width = 75
          Height = 25
          Action = aDodadiSanUslugi
          OptionsImage.Images = dmRes.cxSmallImages
          TabOrder = 1
        end
      end
    end
    object cxTabSheetMED: TcxTabSheet
      Caption = #1052#1080#1082#1088#1086#1073#1080#1086#1083#1086#1075#1080#1112#1072
      ImageIndex = 41
      ExplicitLeft = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object pnl3: TPanel
        Left = 0
        Top = 278
        Width = 962
        Height = 43
        Align = alBottom
        TabOrder = 0
        object btnaIzvadiMedUslugi: TcxButton
          Left = 100
          Top = 10
          Width = 75
          Height = 25
          Action = aIzvadiMedUslugi
          OptionsImage.Images = dmRes.cxSmallImages
          TabOrder = 0
        end
        object btnaDodadiMedUslugi: TcxButton
          Left = 19
          Top = 10
          Width = 75
          Height = 25
          Action = aDodadiMedUslugi
          OptionsImage.Images = dmRes.cxSmallImages
          TabOrder = 1
        end
      end
      object cxgrid3: TcxGrid
        Left = 0
        Top = 0
        Width = 438
        Height = 278
        Align = alClient
        TabOrder = 1
        object cxGrid4DBTableView1: TcxGridDBTableView
          Navigator.Buttons.CustomButtons = <>
          DataController.DataSource = dm.dsDogovoriMedUslugi
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          FilterRow.Visible = True
          OptionsCustomize.ColumnsQuickCustomization = True
          OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
          OptionsData.Deleting = False
          OptionsData.Inserting = False
          OptionsView.GroupByBox = False
          object cxGrid4DBTableView1ID: TcxGridDBColumn
            DataBinding.FieldName = 'ID'
            Visible = False
            Options.Editing = False
            Width = 51
          end
          object cxGrid4DBTableView1DOGOVOR_ID: TcxGridDBColumn
            DataBinding.FieldName = 'DOGOVOR_ID'
            Visible = False
            Options.Editing = False
            Width = 104
          end
          object cxGrid4DBTableView1USLUGA: TcxGridDBColumn
            DataBinding.FieldName = 'USLUGA'
            Visible = False
            Options.Editing = False
            Width = 92
          end
          object cxGrid4DBTableView1USLUGA_SIFRA: TcxGridDBColumn
            DataBinding.FieldName = 'USLUGA_SIFRA'
            Options.Editing = False
            Width = 43
          end
          object cxGrid4DBTableView1USLUGA_NAZIV: TcxGridDBColumn
            DataBinding.FieldName = 'USLUGA_NAZIV'
            Options.Editing = False
            Width = 262
          end
          object cxGrid4DBTableView1CENA: TcxGridDBColumn
            DataBinding.FieldName = 'CENA'
            Styles.Content = dmRes.cxStyle37
            Width = 109
          end
          object cxGrid4DBTableView1CUSTOM1: TcxGridDBColumn
            DataBinding.FieldName = 'CUSTOM1'
            Visible = False
            Options.Editing = False
            Width = 200
          end
          object cxGrid4DBTableView1CUSTOM2: TcxGridDBColumn
            DataBinding.FieldName = 'CUSTOM2'
            Visible = False
            Options.Editing = False
            Width = 200
          end
          object cxGrid4DBTableView1CUSTOM3: TcxGridDBColumn
            DataBinding.FieldName = 'CUSTOM3'
            Visible = False
            Options.Editing = False
            Width = 200
          end
          object cxGrid4DBTableView1TS_INS: TcxGridDBColumn
            DataBinding.FieldName = 'TS_INS'
            Visible = False
            Options.Editing = False
            Width = 200
          end
          object cxGrid4DBTableView1TS_UPD: TcxGridDBColumn
            DataBinding.FieldName = 'TS_UPD'
            Visible = False
            Options.Editing = False
            Width = 200
          end
          object cxGrid4DBTableView1USR_INS: TcxGridDBColumn
            DataBinding.FieldName = 'USR_INS'
            Visible = False
            Options.Editing = False
            Width = 200
          end
          object cxGrid4DBTableView1USR_UPD: TcxGridDBColumn
            DataBinding.FieldName = 'USR_UPD'
            Visible = False
            Options.Editing = False
            Width = 200
          end
        end
        object cxGrid1Grid4Level1: TcxGridLevel
          GridView = cxGrid4DBTableView1
        end
      end
      object pnl4: TPanel
        Left = 438
        Top = 0
        Width = 524
        Height = 278
        Align = alRight
        TabOrder = 2
        object lbl1: TLabel
          Left = 32
          Top = 107
          Width = 146
          Height = 13
          AutoSize = False
          Caption = #1040#1085#1090#1080#1073#1080#1086#1075#1088#1072#1084' '#1082#1083#1072#1089#1080#1095#1077#1085' :'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object lbl2: TLabel
          Left = 32
          Top = 61
          Width = 258
          Height = 13
          AutoSize = False
          Caption = #1048#1076#1077#1085#1090#1080#1092#1080#1082#1072#1094#1080#1112#1072' '#1085#1072' '#1073#1072#1082#1090#1077#1088#1080#1080' '#1072#1074#1090#1086#1084#1072#1090#1089#1082#1080' :'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object CENA_IDENTIFIKACIJA: TcxDBTextEdit
          Tag = 1
          Left = 32
          Top = 80
          Hint = #1041#1088#1086#1112' '#1085#1072' '#1076#1086#1075#1086#1074#1086#1088
          BeepOnEnter = False
          DataBinding.DataField = 'CENA_IDENTIFIKACIJA'
          DataBinding.DataSource = dm.dsDogovori
          Enabled = False
          ParentFont = False
          Properties.BeepOnError = True
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -11
          Style.Font.Name = 'Tahoma'
          Style.Font.Style = []
          Style.Shadow = False
          Style.IsFontAssigned = True
          StyleDisabled.TextColor = clBtnText
          TabOrder = 0
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Width = 249
        end
        object CENA_ANTIBIOGRAM: TcxDBTextEdit
          Tag = 1
          Left = 32
          Top = 126
          Hint = #1041#1088#1086#1112' '#1085#1072' '#1076#1086#1075#1086#1074#1086#1088
          BeepOnEnter = False
          DataBinding.DataField = 'CENA_ANTIBIOGRAM'
          DataBinding.DataSource = dm.dsDogovori
          Enabled = False
          ParentFont = False
          Properties.BeepOnError = True
          Style.Shadow = False
          StyleDisabled.TextColor = clBtnText
          TabOrder = 1
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Width = 249
        end
        object btnaAzurirajDog: TcxButton
          Left = 32
          Top = 14
          Width = 246
          Height = 25
          Action = aAzurirajDog
          OptionsImage.Images = dmRes.cxSmallImages
          TabOrder = 2
          TabStop = False
        end
        object btnZapisiDog: TcxButton
          Left = 124
          Top = 166
          Width = 75
          Height = 25
          Action = aZapisisDog
          OptionsImage.Images = dmRes.cxSmallImages
          TabOrder = 3
        end
        object btnOtkaziDog: TcxButton
          Left = 205
          Top = 166
          Width = 75
          Height = 25
          Action = aOtkaziDog
          OptionsImage.Images = dmRes.cxSmallImages
          TabOrder = 4
        end
      end
    end
  end
  object cxGridPopupMenu1: TcxGridPopupMenu
    Grid = cxGrid1
    PopupMenus = <>
    Left = 440
    Top = 432
  end
  object PopupMenu1: TPopupMenu
    Left = 480
    Top = 488
    object N1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ShortCut = 49235
      OnClick = SaveToIniFileExecute
    end
    object Excel1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1074#1086' Excel'
      ShortCut = 49221
    end
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.Images = dmRes.cxSmallImages
    ImageOptions.LargeImages = dmRes.cxLargeImages
    PopupMenuLinks = <>
    UseSystemFont = True
    Left = 520
    Top = 472
    PixelsPerInch = 96
    object dxBarManager1Bar1: TdxBar
      Caption = 'Custom 1'
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 715
      FloatTop = 157
      FloatClientWidth = 51
      FloatClientHeight = 120
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton1'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton2'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton3'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar2: TdxBar
      Caption = #1040#1082#1094#1080#1080
      CaptionButtons = <>
      DockedLeft = 183
      DockedTop = 0
      FloatLeft = 789
      FloatTop = 120
      FloatClientWidth = 51
      FloatClientHeight = 120
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton10'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton5'
        end
        item
          Visible = True
          ItemName = 'dxBarSubItem1'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar3: TdxBar
      Caption = #1048#1079#1083#1077#1079
      CaptionButtons = <>
      DockedLeft = 428
      DockedTop = 0
      FloatLeft = 718
      FloatTop = 62
      FloatClientWidth = 51
      FloatClientHeight = 80
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton7'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton8'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar4: TdxBar
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 676
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton12'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton15'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton9'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton13'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1BarIzgledGrid: TdxBar
      Caption = #1058#1072#1073#1077#1083#1072
      CaptionButtons = <>
      DockedLeft = 363
      DockedTop = 0
      FloatLeft = 808
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLBtnSnimiIzgled'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLBtnBrisiIzgled'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarLargeButton2: TdxBarLargeButton
      Action = aAzuriraj
      Category = 0
    end
    object dxBarLargeButton3: TdxBarLargeButton
      Action = aBrisi
      Category = 0
    end
    object dxBarLargeButton4: TdxBarLargeButton
      Action = aSnimiIzgled
      Category = 0
      ScreenTip = tipSnimiIzgled
    end
    object dxBarLargeButton5: TdxBarLargeButton
      Action = aZacuvajExcel
      Category = 0
      ScreenTip = tipZacuvajExcel
    end
    object dxBarLargeButton6: TdxBarLargeButton
      Action = aPecatiTabela
      Caption = #1055#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072
      Category = 0
    end
    object dxBarLargeButton7: TdxBarLargeButton
      Action = aHelp
      Category = 0
    end
    object dxBarLargeButton8: TdxBarLargeButton
      Action = aIzlez
      Category = 0
    end
    object dxBarLargeButton1: TdxBarLargeButton
      Action = aNov
      Category = 0
    end
    object dxBarLargeButton9: TdxBarLargeButton
      Action = aSnimiPecatenje
      Category = 0
      ScreenTip = tipSnimiKonf
    end
    object dxBarLargeButton11: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object ribbonFilterReset: TdxBarLargeButton
      Caption = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Category = 0
      Hint = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Visible = ivAlways
    end
    object dxBarLargeButton12: TdxBarLargeButton
      Action = aPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipDizajnReport
    end
    object dxBarLargeButton13: TdxBarLargeButton
      Action = aBrisiPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipBrisiKonf
    end
    object dxBarLargeButton14: TdxBarLargeButton
      Caption = #1048#1079#1073#1088#1080#1096#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 40
    end
    object dxBarLargeButton15: TdxBarLargeButton
      Action = aPageSetup
      Category = 0
      ScreenTip = tipSetiranjeStrana
    end
    object dxBarLargeButton16: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarLargeButton17: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object cxBarEditItem1: TcxBarEditItem
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivAlways
      PropertiesClassName = 'TcxCheckBoxProperties'
    end
    object dxBarSubItem1: TdxBarSubItem
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      Category = 0
      Visible = ivAlways
      ImageIndex = 30
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton6'
        end>
    end
    object dxBarLBtnSnimiIzgled: TdxBarLargeButton
      Action = aSnimiIzgled
      Category = 0
    end
    object dxBarLBtnBrisiIzgled: TdxBarLargeButton
      Action = aBrisiIzgled
      Category = 0
    end
    object dxBarLargeButton10: TdxBarLargeButton
      Action = aSpustiSoberi
      Category = 0
    end
  end
  object ActionList1: TActionList
    Images = dmRes.cxSmallImages
    Left = 272
    Top = 480
    object aNov: TAction
      Caption = #1053#1086#1074
      ImageIndex = 10
      ShortCut = 116
      OnExecute = aNovExecute
    end
    object aAzuriraj: TAction
      Caption = #1040#1078#1091#1088#1080#1088#1072#1112
      ImageIndex = 12
      ShortCut = 117
      OnExecute = aAzurirajExecute
    end
    object aBrisi: TAction
      Caption = #1041#1088#1080#1096#1080
      ImageIndex = 11
      ShortCut = 119
      OnExecute = aBrisiExecute
    end
    object aSnimiIzgled: TAction
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ImageIndex = 9
      ShortCut = 24659
      OnExecute = aSnimiIzgledExecute
    end
    object aZacuvajExcel: TAction
      Caption = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      ImageIndex = 9
      ShortCut = 24645
      OnExecute = aZacuvajExcelExecute
    end
    object aPecatiTabela: TAction
      Caption = #1055#1077#1095#1072#1090#1080
      ImageIndex = 30
      OnExecute = aPecatiTabelaExecute
    end
    object aHelp: TAction
      Caption = #1055#1086#1084#1086#1096
      ImageIndex = 24
    end
    object aIzlez: TAction
      Caption = #1048#1079#1083#1077#1079
      ImageIndex = 25
      OnExecute = aIzlezExecute
    end
    object aZapisi: TAction
      Caption = #1047#1072#1087#1080#1096#1080
      ImageIndex = 7
      ShortCut = 120
      OnExecute = aZapisiExecute
    end
    object aOtkazi: TAction
      Caption = #1054#1090#1082#1072#1078#1080
      ImageIndex = 21
      ShortCut = 27
      OnExecute = aOtkaziExecute
    end
    object aRefresh: TAction
      Caption = 'aRefresh'
      ImageIndex = 18
      ShortCut = 118
      OnExecute = aRefreshExecute
    end
    object aSnimiPecatenje: TAction
      Caption = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 39
      OnExecute = aSnimiPecatenjeExecute
    end
    object aPodesuvanjePecatenje: TAction
      Caption = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      ImageIndex = 38
      OnExecute = aPodesuvanjePecatenjeExecute
    end
    object aBrisiPodesuvanjePecatenje: TAction
      Caption = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 37
      OnExecute = aBrisiPodesuvanjePecatenjeExecute
    end
    object aPageSetup: TAction
      Caption = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      ImageIndex = 38
      OnExecute = aPageSetupExecute
    end
    object aFormConfig: TAction
      Caption = 'aFormConfig'
      ShortCut = 24699
      OnExecute = aFormConfigExecute
    end
    object aBrisiIzgled: TAction
      Caption = #1041#1088#1080#1096#1080' '#1075#1086' '#1079#1072#1095#1091#1074#1072#1085#1080#1086#1090' '#1080#1079#1075#1083#1077#1076
      ImageIndex = 56
      OnExecute = aBrisiIzgledExecute
    end
    object aSpustiSoberi: TAction
      Caption = #1057#1087#1091#1096#1090#1080'/'#1057#1086#1073#1077#1088#1080' '#1058#1072#1073#1077#1083#1072
      ImageIndex = 65
      OnExecute = aSpustiSoberiExecute
    end
    object aDodadiDogovorenOrgan: TAction
      Caption = #1044#1086#1076#1072#1076#1080
      ImageIndex = 26
      OnExecute = aDodadiDogovorenOrganExecute
    end
    object aIzvadiDogovorenOrgan: TAction
      Caption = #1048#1079#1074#1072#1076#1080
      ImageIndex = 46
      OnExecute = aIzvadiDogovorenOrganExecute
    end
    object aDodadiPaket: TAction
      Caption = #1044#1086#1076#1072#1076#1080
      ImageIndex = 26
      OnExecute = aDodadiPaketExecute
    end
    object aIzvadiPAket: TAction
      Caption = #1048#1079#1074#1072#1076#1080
      ImageIndex = 46
      OnExecute = aIzvadiPAketExecute
    end
    object aDodadiMedUslugi: TAction
      Caption = #1044#1086#1076#1072#1076#1080
      ImageIndex = 26
      OnExecute = aDodadiMedUslugiExecute
    end
    object aIzvadiMedUslugi: TAction
      Caption = #1048#1079#1074#1072#1076#1080
      ImageIndex = 46
      OnExecute = aIzvadiMedUslugiExecute
    end
    object aAzurirajDog: TAction
      Caption = #1040#1078#1091#1088#1080#1088#1072#1112
      ImageIndex = 12
      OnExecute = aAzurirajDogExecute
    end
    object aZapisisDog: TAction
      Caption = #1047#1072#1087#1080#1096#1080
      ImageIndex = 7
      OnExecute = aZapisisDogExecute
    end
    object aOtkaziDog: TAction
      Caption = #1054#1090#1082#1072#1078#1080
      ImageIndex = 21
      OnExecute = aOtkaziDogExecute
    end
    object aDodadiSanUslugi: TAction
      Caption = #1044#1086#1076#1072#1076#1080
      ImageIndex = 26
      OnExecute = aDodadiSanUslugiExecute
    end
    object aIzvadiSanUslugi: TAction
      Caption = #1048#1079#1074#1072#1076#1080
      ImageIndex = 46
      OnExecute = aIzvadiSanUslugiExecute
    end
  end
  object dxComponentPrinter1: TdxComponentPrinter
    CurrentLink = dxComponentPrinter1Link1
    Version = 0
    Left = 184
    Top = 544
    object dxComponentPrinter1Link1: TdxGridReportLink
      Component = cxGrid1
      PrinterPage.DMPaper = 9
      PrinterPage.Footer = 6350
      PrinterPage.GrayShading = True
      PrinterPage.Header = 6350
      PrinterPage.Margins.Bottom = 12700
      PrinterPage.Margins.Left = 12700
      PrinterPage.Margins.Right = 12700
      PrinterPage.Margins.Top = 12700
      PrinterPage.Orientation = poLandscape
      PrinterPage.PageFooter.RightTitle.Strings = (
        #1057#1090#1088'.[Page #] '#1086#1076' [Total Pages]')
      PrinterPage.PageSize.X = 210000
      PrinterPage.PageSize.Y = 297000
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      ReportDocument.CreationDate = 40336.425359629630000000
      OptionsFormatting.UseNativeStyles = True
      OptionsOnEveryPage.Caption = False
      OptionsOnEveryPage.FilterBar = False
      OptionsView.Caption = False
      OptionsView.FilterBar = False
      StyleRepository = dmRes.cxStyleRepositoryPrinting
      Styles.StyleSheet = dmRes.dxGridReportLinkStyleSheet
      BuiltInReportLink = True
    end
  end
  object dxBarScreenTipRepository1: TdxBarScreenTipRepository
    Left = 976
    Top = 496
    PixelsPerInch = 96
    object tipSnimiIzgled: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1092#1086#1088#1084#1072#1090#1072' '#1080' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipZacuvajExcel: TdxBarScreenTip
      Header.Text = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      Description.Text = #1057#1085#1080#1084#1080' '#1075#1080' '#1087#1086#1076#1072#1090#1086#1094#1080#1090#1077' '#1086#1076' '#1090#1072#1073#1077#1083#1072#1090#1072' '#1074#1086' Excel '#1092#1086#1088#1084#1072#1090
    end
    object tipDizajnReport: TdxBarScreenTip
      Header.Text = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072' '#1085#1072' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSetiranjeStrana: TdxBarScreenTip
      Header.Text = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1085#1072' '#1082#1086#1112#1072' '#1116#1077' '#1089#1077' '#1087#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSnimiKonf: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
    object tipBrisiKonf: TdxBarScreenTip
      Header.Text = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1048#1079#1073#1088#1080#1096#1080' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
  end
  object cxGridPopupMenu2: TcxGridPopupMenu
    PopupMenus = <>
    Left = 800
    Top = 456
  end
  object cxGridPopupMenu3: TcxGridPopupMenu
    PopupMenus = <>
    Left = 712
    Top = 496
  end
  object cxGridPopupMenu4: TcxGridPopupMenu
    Grid = cxGrid4
    PopupMenus = <>
    Left = 536
    Top = 544
  end
end
