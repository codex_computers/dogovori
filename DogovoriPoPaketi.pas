unit DogovoriPoPaketi;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxEdit,
  DB, cxDBData, cxGridCustomPopupMenu, cxGridPopupMenu,
  ComCtrls, cxContainer, cxTextEdit, cxDBEdit, StdCtrls, cxGridLevel,
  cxClasses, cxControls, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ExtCtrls, Menus, ActnList,
  cxGridExportLink, cxExport, cxLookAndFeelPainters, cxDataStorage,
  cxButtons, dxStatusBar, dxRibbonStatusBar, dxRibbon, dxBar, dxBarExtItems,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSCore, dxPScxCommon,
  cxPC, cxMemo, cxLocalization, cxLookAndFeels,
  dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv,
  dxPSPrVwRibbon, dxPScxEditorProducers, dxPScxExtEditorProducers,
  dxPScxPageControlProducer, dxPgsDlg, cxCheckBox, cxBarEditItem, dxSkinsCore,
  dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue,
  dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, dxSkinscxPCPainter,
  dxBarSkinnedCustForm, cxDropDownEdit, cxCalendar, cxDBLookupComboBox,
  cxMaskEdit, cxLookupEdit, cxDBLookupEdit, cxDBExtLookupComboBox, cxGroupBox,
  cxRadioGroup, dxRibbonSkins, dxPScxGridLnk, dxPScxGridLayoutViewLnk,
  dxScreenTip, dxSkinBlueprint, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinHighContrast, dxSkinSevenClassic,
  dxSkinSharpPlus, dxSkinTheAsphaltWorld, dxSkinVS2010, dxSkinWhiteprint, dmResources,
  dxSkinOffice2013White, System.Actions, cxNavigator, cxPCdxBarPopupMenu,
  dxSkinMetropolis, dxSkinMetropolisDark, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2016Colorful, dxSkinOffice2016Dark,
  dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxRibbonCustomizationForm, dxBarBuiltInMenu,
  cxImageComboBox;

type
//  niza = Array[1..5] of Variant;

  TfrmDogovoriMedUslugi = class(TForm)
    cxGridPopupMenu1: TcxGridPopupMenu;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    Excel1: TMenuItem;
    dxRibbon1Tab1: TdxRibbonTab;
    dxRibbon1: TdxRibbon;
    StatusBar1: TdxRibbonStatusBar;
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar1: TdxBar;
    dxBarManager1Bar2: TdxBar;
    dxBarManager1Bar3: TdxBar;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarLargeButton3: TdxBarLargeButton;
    dxBarLargeButton4: TdxBarLargeButton;
    dxBarLargeButton5: TdxBarLargeButton;
    dxBarLargeButton6: TdxBarLargeButton;
    dxBarLargeButton7: TdxBarLargeButton;
    dxBarLargeButton8: TdxBarLargeButton;
    dxBarLargeButton1: TdxBarLargeButton;
    ActionList1: TActionList;
    aNov: TAction;
    aAzuriraj: TAction;
    aBrisi: TAction;
    aSnimiIzgled: TAction;
    aZacuvajExcel: TAction;
    aPecatiTabela: TAction;
    aHelp: TAction;
    aIzlez: TAction;
    aZapisi: TAction;
    aOtkazi: TAction;
    aRefresh: TAction;
    dxRibbon1Tab2: TdxRibbonTab;
    dxBarManager1Bar4: TdxBar;
    dxBarLargeButton9: TdxBarLargeButton;
    aSnimiPecatenje: TAction;
    dxComponentPrinter1: TdxComponentPrinter;
    dxComponentPrinter1Link1: TdxGridReportLink;
    dxBarLargeButton11: TdxBarLargeButton;
    ribbonFilterReset: TdxBarLargeButton;
    aPodesuvanjePecatenje: TAction;
    dxBarLargeButton12: TdxBarLargeButton;
    aBrisiPodesuvanjePecatenje: TAction;
    dxBarLargeButton13: TdxBarLargeButton;
    dxBarLargeButton14: TdxBarLargeButton;
    aPageSetup: TAction;
    dxBarLargeButton15: TdxBarLargeButton;
    dxBarLargeButton16: TdxBarLargeButton;
    dxBarLargeButton17: TdxBarLargeButton;
    cxBarEditItem1: TcxBarEditItem;
    dxBarSubItem1: TdxBarSubItem;
    aFormConfig: TAction;
    dxBarScreenTipRepository1: TdxBarScreenTipRepository;
    tipSnimiIzgled: TdxBarScreenTip;
    tipZacuvajExcel: TdxBarScreenTip;
    tipDizajnReport: TdxBarScreenTip;
    tipSetiranjeStrana: TdxBarScreenTip;
    tipSnimiKonf: TdxBarScreenTip;
    tipBrisiKonf: TdxBarScreenTip;
    dxBarManager1BarIzgledGrid: TdxBar;
    dxBarLBtnSnimiIzgled: TdxBarLargeButton;
    dxBarLBtnBrisiIzgled: TdxBarLargeButton;
    aBrisiIzgled: TAction;
    aSpustiSoberi: TAction;
    dxBarLargeButton10: TdxBarLargeButton;
    Panel1: TPanel;
    PanelDogovor: TPanel;
    Panel3: TPanel;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxButton1: TcxButton;
    aDodadiDogovorenOrgan: TAction;
    aIzvadiDogovorenOrgan: TAction;
    PARTNERNAZIV: TcxExtLookupComboBox;
    PARTNER: TcxDBTextEdit;
    TIP_PARTNER: TcxDBTextEdit;
    Label14: TLabel;
    Label6: TLabel;
    BROJ: TcxDBTextEdit;
    cxGroupBox1: TcxGroupBox;
    Label11: TLabel;
    Label12: TLabel;
    DATUM_DO: TcxDBDateEdit;
    DATUM_OD: TcxDBDateEdit;
    cxButton8: TcxButton;
    cxButton9: TcxButton;
    cxGrid1DBTableView1ID: TcxGridDBColumn;
    cxGrid1DBTableView1BROJ: TcxGridDBColumn;
    cxGrid1DBTableView1TIP_PARTNER: TcxGridDBColumn;
    cxGrid1DBTableView1PARTNER: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV_SKRATEN: TcxGridDBColumn;
    cxGrid1DBTableView1OPIS: TcxGridDBColumn;
    cxGrid1DBTableView1CUSTOM1: TcxGridDBColumn;
    cxGrid1DBTableView1CUSTOM2: TcxGridDBColumn;
    cxGrid1DBTableView1CUSTOM3: TcxGridDBColumn;
    cxGrid1DBTableView1TS_INS: TcxGridDBColumn;
    cxGrid1DBTableView1TS_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1USR_INS: TcxGridDBColumn;
    cxGrid1DBTableView1USR_UPD: TcxGridDBColumn;
    aDodadiPaket: TAction;
    aIzvadiPAket: TAction;
    cxGridPopupMenu2: TcxGridPopupMenu;
    cxGridPopupMenu3: TcxGridPopupMenu;
    cxGrid1DBTableView1DATUM_OD: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM_DO: TcxGridDBColumn;
    Label1: TLabel;
    OPIS: TcxDBMemo;
    aDodadiMedUslugi: TAction;
    aIzvadiMedUslugi: TAction;
    aAzurirajDog: TAction;
    aZapisisDog: TAction;
    aOtkaziDog: TAction;
    cxGrid1DBTableView1CENA_IDENTIFIKACIJA: TcxGridDBColumn;
    cxGrid1DBTableView1CENA_ANTIBIOGRAM: TcxGridDBColumn;
    cxGroupBox2: TcxGroupBox;
    Label4: TLabel;
    BROJ_DENOVI_FAKTURA: TcxDBTextEdit;
    cxGrid1DBTableView1BROJ_DENOVI_FAKTURA: TcxGridDBColumn;
    cxPageControl1: TcxPageControl;
    cxTabSheetPartneri: TcxTabSheet;
    pnl1: TPanel;
    btnaDodadiDogovorenOrgan: TcxButton;
    btnaIzvadiDogovorenOrgan: TcxButton;
    cxgrid5: TcxGrid;
    cxGrid2DBTableView1: TcxGridDBTableView;
    cxGrid2DBTableView1ID: TcxGridDBColumn;
    cxGrid2DBTableView1DOGOVOR_ID: TcxGridDBColumn;
    cxGrid2DBTableView1TIP_PARTNER: TcxGridDBColumn;
    cxGrid2DBTableView1PARTNER: TcxGridDBColumn;
    cxGrid2DBTableView1NAZIV: TcxGridDBColumn;
    cxGrid2DBTableView1NAZIV_SKRATEN: TcxGridDBColumn;
    cxGrid2DBTableView1OPIS: TcxGridDBColumn;
    cxGrid2DBTableView1CUSTOM1: TcxGridDBColumn;
    cxGrid2DBTableView1CUSTOM2: TcxGridDBColumn;
    cxGrid2DBTableView1CUSTOM3: TcxGridDBColumn;
    cxGrid2DBTableView1TS_INS: TcxGridDBColumn;
    cxGrid2DBTableView1TS_UPD: TcxGridDBColumn;
    cxGrid2DBTableView1USR_INS: TcxGridDBColumn;
    cxGrid2DBTableView1USR_UPD: TcxGridDBColumn;
    cxGrid1Grid2Level1: TcxGridLevel;
    cxTabSheetSANPaketi: TcxTabSheet;
    pnl2: TPanel;
    btnaIzvadiPAket: TcxButton;
    btnaDodadiPaket: TcxButton;
    cxgrid2: TcxGrid;
    cxGrid3DBTableView1: TcxGridDBTableView;
    cxGrid3DBTableView1ID: TcxGridDBColumn;
    cxGrid3DBTableView1DOGOVOR_ID: TcxGridDBColumn;
    cxGrid3DBTableView1PAKET: TcxGridDBColumn;
    cxGrid3DBTableView1PAKETNAZIV: TcxGridDBColumn;
    cxGrid3DBTableView1CENA: TcxGridDBColumn;
    cxGrid3DBTableView1TS_INS: TcxGridDBColumn;
    cxGrid3DBTableView1TS_UPD: TcxGridDBColumn;
    cxGrid3DBTableView1USR_INS: TcxGridDBColumn;
    cxGrid3DBTableView1USR_UPD: TcxGridDBColumn;
    cxGrid3DBTableView1CUSTOM1: TcxGridDBColumn;
    cxGrid3DBTableView1CUSTOM2: TcxGridDBColumn;
    cxGrid3DBTableView1CUSTOM3: TcxGridDBColumn;
    cxGrid1Grid3Level1: TcxGridLevel;
    cxTabSheetMED: TcxTabSheet;
    pnl3: TPanel;
    btnaIzvadiMedUslugi: TcxButton;
    btnaDodadiMedUslugi: TcxButton;
    cxgrid3: TcxGrid;
    cxGrid4DBTableView1: TcxGridDBTableView;
    cxGrid4DBTableView1ID: TcxGridDBColumn;
    cxGrid4DBTableView1DOGOVOR_ID: TcxGridDBColumn;
    cxGrid4DBTableView1USLUGA: TcxGridDBColumn;
    cxGrid4DBTableView1USLUGA_SIFRA: TcxGridDBColumn;
    cxGrid4DBTableView1USLUGA_NAZIV: TcxGridDBColumn;
    cxGrid4DBTableView1CENA: TcxGridDBColumn;
    cxGrid4DBTableView1CUSTOM1: TcxGridDBColumn;
    cxGrid4DBTableView1CUSTOM2: TcxGridDBColumn;
    cxGrid4DBTableView1CUSTOM3: TcxGridDBColumn;
    cxGrid4DBTableView1TS_INS: TcxGridDBColumn;
    cxGrid4DBTableView1TS_UPD: TcxGridDBColumn;
    cxGrid4DBTableView1USR_INS: TcxGridDBColumn;
    cxGrid4DBTableView1USR_UPD: TcxGridDBColumn;
    cxGrid1Grid4Level1: TcxGridLevel;
    pnl4: TPanel;
    lbl1: TLabel;
    lbl2: TLabel;
    CENA_IDENTIFIKACIJA: TcxDBTextEdit;
    CENA_ANTIBIOGRAM: TcxDBTextEdit;
    btnaAzurirajDog: TcxButton;
    btnZapisiDog: TcxButton;
    btnOtkaziDog: TcxButton;
    cxTabSheetSANUslugi: TcxTabSheet;
    cxGrid4: TcxGrid;
    cxGrid1DBTableView2: TcxGridDBTableView;
    cxGridLevel1: TcxGridLevel;
    cxGrid1DBTableView2ID: TcxGridDBColumn;
    cxGrid1DBTableView2DOGOVOR_ID: TcxGridDBColumn;
    cxGrid1DBTableView2USLUGA_ID: TcxGridDBColumn;
    cxGrid1DBTableView2USLUGANAZIV: TcxGridDBColumn;
    cxGrid1DBTableView2TIP: TcxGridDBColumn;
    cxGrid1DBTableView2CENA: TcxGridDBColumn;
    cxGrid1DBTableView2TS_INS: TcxGridDBColumn;
    cxGrid1DBTableView2TS_UPD: TcxGridDBColumn;
    cxGrid1DBTableView2USR_INS: TcxGridDBColumn;
    cxGrid1DBTableView2USR_UPD: TcxGridDBColumn;
    cxGrid1DBTableView2CUSTOM1: TcxGridDBColumn;
    cxGrid1DBTableView2CUSTOM2: TcxGridDBColumn;
    cxGrid1DBTableView2CUSTOM3: TcxGridDBColumn;
    pnl5: TPanel;
    btnaIzvadiPAket1: TcxButton;
    btnaDodadiPaket1: TcxButton;
    aDodadiSanUslugi: TAction;
    aIzvadiSanUslugi: TAction;
    cxGridPopupMenu4: TcxGridPopupMenu;

    procedure cxDBTextEditAllEnter(Sender: TObject);
    procedure cxDBTextEditAllExit(Sender: TObject);
    procedure EnterKakoTab(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    //procedure PrvPosledenTab(panel:TPanel;var posledna,prva:TWinControl);
    procedure SaveToIniFileExecute(Sender: TObject);
    procedure cxGrid1DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure aZapisiExecute(Sender: TObject);
    procedure aOtkaziExecute(Sender: TObject);
    procedure aNovExecute(Sender: TObject);
    procedure aAzurirajExecute(Sender: TObject);
    procedure aBrisiExecute(Sender: TObject);
    procedure aZacuvajExcelExecute(Sender: TObject);
    procedure aRefreshExecute(Sender: TObject);
    procedure aSnimiIzgledExecute(Sender: TObject);
    procedure aIzlezExecute(Sender: TObject);
    procedure aPecatiTabelaExecute(Sender: TObject);
    procedure aSnimiPecatenjeExecute(Sender: TObject);
    procedure zacuvajPrintVoIni(ime:AnsiString);
    procedure procitajPrintOdIni(ime:AnsiString);
    procedure aPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aPageSetupExecute(Sender: TObject);
    procedure aFormConfigExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure PartnerPropertiesChange(Sender: TObject);
    procedure aBrisiIzgledExecute(Sender: TObject);
    procedure aSpustiSoberiExecute(Sender: TObject);
    procedure aDodadiDogovorenOrganExecute(Sender: TObject);
    procedure aIzvadiDogovorenOrganExecute(Sender: TObject);
    procedure PARTNERNAZIVPropertiesCloseUp(Sender: TObject);
    procedure ZemiImeDvoenKluc(tip:TcxDBTextEdit; sifra:TcxDBTextEdit; lukap:TcxExtLookupComboBox);
    procedure cxGrid1DBTableView1FocusedRecordChanged(
      Sender: TcxCustomGridTableView; APrevFocusedRecord,
      AFocusedRecord: TcxCustomGridRecord;
      ANewItemRecordFocusingChanged: Boolean);
    procedure aDodadiPaketExecute(Sender: TObject);
    procedure aIzvadiPAketExecute(Sender: TObject);
    procedure cxGrid2DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure cxGrid3DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure proveriDatum(datum: TcxDBDateEdit);
    procedure proveriDatumDogOrgan(datum: TcxDBDateEdit);
    procedure aDodadiMedUslugiExecute(Sender: TObject);
    procedure aIzvadiMedUslugiExecute(Sender: TObject);
    procedure aAzurirajDogExecute(Sender: TObject);
    procedure aZapisisDogExecute(Sender: TObject);
    procedure aOtkaziDogExecute(Sender: TObject);
    procedure aDodadiSanUslugiExecute(Sender: TObject);
    procedure aIzvadiSanUslugiExecute(Sender: TObject);
    procedure cxGrid1DBTableView2KeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }

    _sifra_kluc :array[1..10] of string;  // �������� ���������� �� ������ �� ������� �� ������������� ���

  protected
    list : TList;
    inserting, sobrano : boolean;
    prva, posledna :TWinControl;
    procedure prefrli;

  public
    { Public declarations }
    constructor Create(Owner : TComponent; insert : boolean = true);reintroduce; overload;

    procedure SetSifra(br: integer; s : string);
    function GetSifra(br: integer) : string;

    //������� �� ���������� ������������ _sifra_kluc
    property sifra_kluc[br :integer]: string read GetSifra write SetSifra;
  end;

var
  frmDogovoriMedUslugi: TfrmDogovoriMedUslugi;
  rData : TRepositoryData;

implementation

uses DaNe, dmKonekcija, Utils, FormConfig, NurkoRepository, dmUnit, dmMaticni,
  Partner, sifSanPaketi, sifMedUslugi, sifSanUslugi;

{$R *.dfm}
//------------------------------------------------------------------------------

constructor TfrmDogovoriMedUslugi.Create(Owner : TComponent; insert : boolean = true);
begin
inherited Create(Owner);
//	�������� ���� ������� � �������� �� ����������� ���������
//	insert = true -> ����� ���� ����� ���������� �� ������� �� Insert Mode
//  insert = false -> ����� ���� ����� ������� �� ����� �� ������. �� ������� ����� ����� �� �� �������� F5
  inserting := insert;
end;

//	����� �� �����������. �� ��������� ������� �� ������, �� �������� ������� �� ���� ��������,
//	�� ������� ������� �� ������ ������ � �� ������� �� Insert Mode
procedure TfrmDogovoriMedUslugi.aNovExecute(Sender: TObject);
begin
  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
  begin
    PanelDogovor.Enabled:=True;
    TIP_PARTNER.SetFocus;
    cxGrid1DBTableView1.DataController.DataSet.Insert;
    ZemiImeDvoenKluc(TIP_PARTNER,PARTNER, PARTNERNAZIV);
  end
  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������,' + sLineBreak + '� ����� ������� �� ������� ��� �����!');
end;

//	����� �� ���������. �� ��������� ������� �� ������, �� �������� ������� �� ���� ��������,
//	�� ������� ������� �� ������ ������ � �� ������� �� Edit Mode
procedure TfrmDogovoriMedUslugi.aAzurirajDogExecute(Sender: TObject);
begin
  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
  begin
    CENA_IDENTIFIKACIJA.Enabled:=True;
    CENA_ANTIBIOGRAM.Enabled:=True;
    btnZapisiDog.Enabled:=True;
    btnOtkaziDog.Enabled:=True;
    CENA_IDENTIFIKACIJA.SetFocus;
    cxGrid1DBTableView1.DataController.DataSet.Edit;
  end
  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������,' + sLineBreak + '� ����� ������� �� ������� ��� �����!');
end;

procedure TfrmDogovoriMedUslugi.aAzurirajExecute(Sender: TObject);
begin

  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
  begin
    dm.qCountDogovorVoSan.Close;
    dm.qCountDogovorVoSan.ParamByName('dogovor_id').Value:=dm.tblDogovoriID.Value;
    dm.qCountDogovorVoSan.ExecQuery;
    dm.qCountDogovorVoMED.Close;
    dm.qCountDogovorVoMED.ParamByName('dogovor_id').Value:=dm.tblDogovoriID.Value;
    dm.qCountDogovorVoMED.ExecQuery;

    if dm.qCountDogovorVoMED.FldByName['br'].Value = 0 then
       if dm.qCountDogovorVoSan.FldByName['br'].Value = 0 then
          begin
            PanelDogovor.Enabled:=True;
            TIP_PARTNER.SetFocus;
            cxGrid1DBTableView1.DataController.DataSet.Edit;
          end
       else ShowMessage('��������� ������ �� �� �������, ��������� � ��������� �� ��������� �� ���� �� ��������� ������ !!!')
    else ShowMessage('��������� ������ �� �� �������, ��������� � ��������� �� ��������� �� ���� �� ������������� ������ !!!');
  end
  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������,' + sLineBreak + '� ����� ������� �� ������� ��� �����!');
end;

//	����� �� ������ �� ������������� �����
procedure TfrmDogovoriMedUslugi.aBrisiExecute(Sender: TObject);
begin
  if ((cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) and
     (cxGrid1DBTableView1.DataController.RecordCount <> 0)) then
    cxGrid1DBTableView1.DataController.DataSet.Delete();
end;

procedure TfrmDogovoriMedUslugi.aBrisiIzgledExecute(Sender: TObject);
begin
  brisiGridVoBaza(Name,cxGrid1DBTableView1);
  //brisiPivotVoBaza(Name,cxDBPivotGrid1); �� pivot ��� ���
  BrisiFormaIzgled(self);
end;

//	����� �� ���������� �� ����������
procedure TfrmDogovoriMedUslugi.aRefreshExecute(Sender: TObject);
begin
//  cxGrid1DBTableView1.DataController.Filter.Root.Clear; //���������� �� ��������
//  cxGrid1DBTableView1.DataController.DataSet.Refresh;
end;

//	����� �� ����� �� ������� �� ������� �� ������� �����
procedure TfrmDogovoriMedUslugi.aIzlezExecute(Sender: TObject);
begin
  Close;
end;

procedure TfrmDogovoriMedUslugi.aIzvadiDogovorenOrganExecute(Sender: TObject);
begin
if ((cxGrid2DBTableView1.DataController.DataSource.State = dsBrowse) and
     (cxGrid2DBTableView1.DataController.RecordCount <> 0)) then
    cxGrid2DBTableView1.DataController.DataSet.Delete();
end;

procedure TfrmDogovoriMedUslugi.aIzvadiMedUslugiExecute(Sender: TObject);
begin
  if ((cxGrid4DBTableView1.DataController.DataSource.State = dsBrowse) and
     (cxGrid4DBTableView1.DataController.RecordCount <> 0)) then
    cxGrid4DBTableView1.DataController.DataSet.Delete();
end;

procedure TfrmDogovoriMedUslugi.aIzvadiPAketExecute(Sender: TObject);
begin
  if ((cxGrid3DBTableView1.DataController.DataSource.State = dsBrowse) and
     (cxGrid3DBTableView1.DataController.RecordCount <> 0)) then
    cxGrid3DBTableView1.DataController.DataSet.Delete();
end;

procedure TfrmDogovoriMedUslugi.aIzvadiSanUslugiExecute(Sender: TObject);
begin
  if ((cxGrid1DBTableView2.DataController.DataSource.State = dsBrowse) and
     (cxGrid1DBTableView2.DataController.RecordCount <> 0)) then
    cxGrid1DBTableView2.DataController.DataSet.Delete();
end;

//	����� �� ������� �� �������� �� ������ �� ���� (Utils.pas)
procedure TfrmDogovoriMedUslugi.aSnimiIzgledExecute(Sender: TObject);
begin
  zacuvajGridVoBaza(Name,cxGrid1DBTableView1);
  ZacuvajFormaIzgled(self);
end;

//	����� �� ������� �� ������ �� Excel ������ (Utils.pas)
procedure TfrmDogovoriMedUslugi.aZacuvajExcelExecute(Sender: TObject);
begin
  zacuvajVoExcel(cxGrid1, Caption);
end;

//  ��������� �� ������ �� ������� �� ���������� �� ����� �� Enter
procedure TfrmDogovoriMedUslugi.EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  kom : TWinControl;
begin
    kom := Sender as TWinControl;
    case Key of
        VK_DOWN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
          key:=0;
        end;
        VK_UP:
        begin
          PostMessage(Handle,WM_NextDlgCtl,1,0);
          key:=0;
        end;
        VK_RETURN:
        begin
          if Sender = OPIS then
             cxGroupBox1.SetFocus;
          PostMessage(Handle,WM_NextDlgCtl,0,0);
        end;
        VK_INSERT:
        begin
          //  �� ����� ��������
          if (kom = PARTNERNAZIV)  then
          begin
            frmNurkoRepository := TfrmNurkoRepository.Create(nil,Name,rData);
            frmNurkoRepository.kontrola_naziv := kom.Name;
            frmNurkoRepository.ShowModal;

            if (frmNurkoRepository.ModalResult = mrOk) then
              PARTNERNAZIV.EditValue := frmNurkoRepository.cxGrid1Level1.GridView.DataController.GetRecordId(frmNurkoRepository.cxGrid1Level1.GridView.DataController.FocusedRecordIndex);
            if PARTNERNAZIV.Text <> '' then
               begin
                 dm.tblDogovoriTIP_PARTNER.Value:=PARTNERNAZIV.EditValue[0];
                 dm.tblDogovoriPARTNER.Value:=PARTNERNAZIV.EditValue[1];
               end
            else
               begin
                 PARTNER.Clear;
                 TIP_PARTNER.Clear;
               end;
            frmNurkoRepository.Free;
       	 end;
	      end;
    end;
end;

//	������� �� ��� �� ������ ��� �������� �� �����
procedure TfrmDogovoriMedUslugi.cxDBTextEditAllEnter(Sender: TObject);
begin
    TEdit(Sender).Color:=clSkyBlue;

end;

//	������� �� ��� �� ������ ��� ����� �� �����
procedure TfrmDogovoriMedUslugi.cxDBTextEditAllExit(Sender: TObject);
var
  kom : TWinControl;
begin
    kom := Sender as TWinControl;
    TEdit(Sender).Color:=clWhite;
    if (cxGrid1DBTableView1.DataController.DataSource.State = dsEdit) or (cxGrid1DBTableView1.DataController.DataSource.State = dsInsert) then
       begin
         if ((Sender as TWinControl)= TIP_PARTNER) or ((Sender as TWinControl)= PARTNER) then
           begin
               ZemiImeDvoenKluc(TIP_PARTNER,PARTNER, PARTNERNAZIV);
           end;
         if (kom = DATUM_OD) then
            proveriDatum(DATUM_OD)
         else if (kom = DATUM_DO)then
            proveriDatum(DATUM_DO)
         else if ((kom = PARTNER) or (kom = TIP_PARTNER))then
            proveriDatum(DATUM_DO);

      end;

end;

procedure TfrmDogovoriMedUslugi.cxGrid1DBTableView1FocusedRecordChanged(
  Sender: TcxCustomGridTableView; APrevFocusedRecord,
  AFocusedRecord: TcxCustomGridRecord; ANewItemRecordFocusingChanged: Boolean);
begin
   ZemiImeDvoenKluc(TIP_PARTNER,PARTNER, PARTNERNAZIV);
end;

//  ��������� �� ������ �� ��������� �� ���������� ���� �� ��������
//  ��������� ��: br - ����� ��� �� ������ (�� 0 �� 9) �� ��������� �� ����� �� ����� �� ���������� ����
//  s - ��������� �� ������ �� ������ (���������� �� string �� ����� �� integer �� IntToStr ���������)
procedure TfrmDogovoriMedUslugi.SetSifra(br: integer; s : string);
begin
  _sifra_kluc[br] := s;
end;

//  ������� �� ������ �� ��������� �� ���������� ���� �� ��������
//  ��������� ��: br - ����� ��� �� ������ (�� 0 �� 9) �� ��������� �� ����� �� ����� �� ���������� ����
function TfrmDogovoriMedUslugi.GetSifra(br: integer) : string;
begin
  Result := _sifra_kluc[br];
end;

procedure TfrmDogovoriMedUslugi.PARTNERNAZIVPropertiesCloseUp(Sender: TObject);
begin
if (cxGrid1DBTableView1.DataController.DataSource.State = dsEdit) or (cxGrid1DBTableView1.DataController.DataSource.State = dsInsert) then
    begin
      if PARTNERNAZIV.Text <> '' then
         begin
           dm.tblDogovoriTIP_PARTNER.Value:=PARTNERNAZIV.EditValue[0];
           dm.tblDogovoriPARTNER.Value:=PARTNERNAZIV.EditValue[1];
         end
      else
         begin
           PARTNER.Clear;
           TIP_PARTNER.Clear;
         end;
    end;
end;

procedure TfrmDogovoriMedUslugi.PartnerPropertiesChange(Sender: TObject);
begin

end;

//  �� ������� �� ������� ��� ����������� �� Master-�� �� �������� �� ��������� �� ������ ����� SetSifra
procedure TfrmDogovoriMedUslugi.prefrli;
begin
end;

procedure TfrmDogovoriMedUslugi.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    //	��������� ��� ��������� �� �������
    if (cxGrid1DBTableView1.DataController.DataSource.State = dsEdit) or (cxGrid1DBTableView1.DataController.DataSource.State = dsInsert) then
    begin
        frmDaNe := TfrmDaNe.Create(self, '���������� ��������', '���������� �� �� ��������. ���� ������ �� �� �������?', 1);
        if (frmDaNe.ShowModal <> mrYes) then
        begin
            cxGrid1DBTableView1.DataController.DataSet.Cancel;
            Action := caFree;
        end
        else
          if (Validacija(PanelDogovor) = false) then
          begin
            cxGrid1DBTableView1.DataController.DataSet.Post;
            Action := caFree;
          end
          else Action := caNone;
    end;
//  dm.tblDogovori.Close;
//  dm.tblDogovorniOrgani.Close;
//  dm.tblDogovoriSanPaketi.Close;
  dmRes.FreeRepository(rData);
end;
procedure TfrmDogovoriMedUslugi.FormCreate(Sender: TObject);
begin
  ProcitajFormaIzgled(self);
  if (dmRes.use_skin = true) then dxRibbon1.ColorSchemeName := dmRes.skin_name;

  rData := TRepositoryData.Create();  // kreirame instance od klasata TRepositoryData sto se naogja vo dmRes
end;

//------------------------------------------------------------------------------

procedure TfrmDogovoriMedUslugi.FormShow(Sender: TObject);
begin

    SpremiForma(self);

    dxBarManager1Bar1.Caption := Caption;
    dxComponentPrinter1Link1.ReportTitle.Text := Caption;

    procitajGridOdBaza(Name,cxGrid1DBTableView1,false,false);
    procitajPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
    sobrano := true;

    ZemiImeDvoenKluc(TIP_PARTNER,PARTNER, PARTNERNAZIV);

    PARTNERNAZIV.RepositoryItem := dmRes.InitRepository(-1, 'PARTNERNAZIV', Name, rData);

    cxPageControl1.ActivePage:=cxTabSheetPartneri;
 end;
//------------------------------------------------------------------------------

procedure TfrmDogovoriMedUslugi.SaveToIniFileExecute(Sender: TObject);
begin
//    cxGrid1DBTableView1.StoreToIniFile(Name,True,[]);
end;
//------------------------------------------------------------------------------

procedure TfrmDogovoriMedUslugi.cxGrid1DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
  if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid1DBTableView1);
end;

procedure TfrmDogovoriMedUslugi.cxGrid1DBTableView2KeyPress(Sender: TObject;
  var Key: Char);
begin
     if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid1DBTableView2);
end;

procedure TfrmDogovoriMedUslugi.cxGrid2DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
  if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid2DBTableView1);
end;

procedure TfrmDogovoriMedUslugi.cxGrid3DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
  if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid3DBTableView1);
end;

//  ����� �� �����
procedure TfrmDogovoriMedUslugi.aZapisiExecute(Sender: TObject);
var
  st: TDataSetState;
begin
//  ZapisiButton.SetFocus;

  st := cxGrid1DBTableView1.DataController.DataSet.State;
  if st in [dsEdit,dsInsert] then
  begin
    if (Validacija(PanelDogovor) = false) then
    begin
       if dm.tblDogovoriDATUM_OD.Value <= dm.tblDogovoriDATUM_DO.Value then
           begin
              cxGrid1DBTableView1.DataController.DataSet.Post;
              cxGrid1DBTableView1.DataController.Refresh;
              PanelDogovor.Enabled:=false;
              cxGrid1.SetFocus;
           end
       else
           begin
              ShowMessage('��������� ������ !!!');
              DATUM_OD.SetFocus;
           end;
    end;
  end;
end;

procedure TfrmDogovoriMedUslugi.aZapisisDogExecute(Sender: TObject);
var
  st: TDataSetState;
begin
//  ZapisiButton.SetFocus;

  st := cxGrid1DBTableView1.DataController.DataSet.State;
  if st in [dsEdit,dsInsert] then
  begin
    if (Validacija(Pnl2) = false) then
    begin
       cxGrid1DBTableView1.DataController.DataSet.Post;
       CENA_IDENTIFIKACIJA.Enabled:=False;
       CENA_ANTIBIOGRAM.Enabled:=False;
       btnZapisiDog.Enabled:=False;
       btnOtkaziDog.Enabled:=False;
    end;
  end;
end;

//	����� �� ���������� �� �������
procedure TfrmDogovoriMedUslugi.aOtkaziDogExecute(Sender: TObject);
var
  st: TDataSetState;
begin
//  ZapisiButton.SetFocus;

  st := cxGrid1DBTableView1.DataController.DataSet.State;
  if st in [dsEdit,dsInsert] then
  begin
       cxGrid1DBTableView1.DataController.DataSet.Cancel;
       CENA_IDENTIFIKACIJA.Enabled:=False;
       CENA_ANTIBIOGRAM.Enabled:=False;
       btnZapisiDog.Enabled:=False;
       btnOtkaziDog.Enabled:=False;
  end;
end;

procedure TfrmDogovoriMedUslugi.aOtkaziExecute(Sender: TObject);
begin
  if (cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
  begin
      ModalResult := mrCancel;
      Close();
  end
  else
  begin
      cxGrid1DBTableView1.DataController.DataSet.Cancel;
      RestoreControls(PanelDogovor);
      PanelDogovor.Enabled := false;
      cxGrid1.SetFocus;
  end;
end;

//----------------------------------------------------------------------------------
// ��������� �� ��������, ���������� �� ��������� � �������� �� Print ��������
//----------------------------------------------------------------------------------

//	����� �� �������� �� �������� �� �������
procedure TfrmDogovoriMedUslugi.aPageSetupExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.PageSetup;
end;

//	����� �� ������� �� ������
procedure TfrmDogovoriMedUslugi.aPecatiTabelaExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.ReportTitle.Text := Caption;

  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Clear;
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(dmkon.firma_naziv);
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(DateTimeToStr(Now));

  dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Clear;
  //dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Add('������� : ' + TipSifra.Text + '-' + IdSifra.Text + ' ' + Partner.Text);

  dxComponentPrinter1.Preview(true, dxComponentPrinter1Link1);
end;

//	����� �� ���������� �� ��������� �� �������
procedure TfrmDogovoriMedUslugi.aPodesuvanjePecatenjeExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.DesignReport();
end;

procedure TfrmDogovoriMedUslugi.aSnimiPecatenjeExecute(Sender: TObject);
begin
 zacuvajPrintVoBaza(Name,cxGrid1DBTableView1.Name,dxComponentPrinter1Link1);
end;

//	����� �� �������� ��� �������� �� �������� �� ������ ���� ��� ��������� �� ����� ������
procedure TfrmDogovoriMedUslugi.aSpustiSoberiExecute(Sender: TObject);
begin
  if (sobrano = true) then
  begin
    cxGrid1DBTableView1.ViewData.Expand(false);
    sobrano := false;
  end
  else
  begin
    cxGrid1DBTableView1.ViewData.Collapse(false);
    sobrano := true;
  end;
end;

//	����� �� ������ �� ����������� �������� �� ������� (Utils.pas)
procedure TfrmDogovoriMedUslugi.aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
begin
 brisiPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
end;

procedure TfrmDogovoriMedUslugi.aDodadiDogovorenOrganExecute(Sender: TObject);
begin
  frmPartner:=TfrmPartner.Create(self,false);
  frmPartner.ShowModal;
  if (frmPartner.ModalResult = mrOK) then
    begin

        dm.qAktivenDogovorDo.Close;
        dm.qAktivenDogovorDo.ParamByName('datum_od').Value:=dm.tblDogovoriDATUM_OD.Value;
        dm.qAktivenDogovorDo.ParamByName('datum_do').Value:=dm.tblDogovoriDATUM_DO.Value;
        dm.qAktivenDogovorDo.ParamByName('tip_partner').Value:=StrToInt(frmPartner.GetSifra(0));
        dm.qAktivenDogovorDo.ParamByName('partner').Value:=StrToInt(frmPartner.GetSifra(1));
        dm.qAktivenDogovorDo.ExecQuery;
        if (dm.qAktivenDogovorDo.FldByName['br'].Value = 1)then
           begin
             ShowMessage('��� ������������ �� ������ �� ������� �� ��������� �������!!!');
           end
        else
           begin
            dm.tblDogovorniOrgani.Insert;
            dm.tblDogovorniOrganiTIP_PARTNER.Value:= StrToInt(frmPartner.GetSifra(0));
            dm.tblDogovorniOrganiPARTNER.Value:= StrToInt(frmPartner.GetSifra(1));
            dm.tblDogovorniOrganiDOGOVOR_ID.Value:=dm.tblDogovoriID.Value;
            dm.tblDogovorniOrgani.Post;
            dm.tblDogovorniOrgani.FullRefresh;
           end;
    end;
  frmPartner.Free;

end;

procedure TfrmDogovoriMedUslugi.aDodadiMedUslugiExecute(Sender: TObject);
begin
  frmMedUslugii:=TfrmMedUslugii.Create(self,false);
  frmMedUslugii.ShowModal;
  if (frmMedUslugii.ModalResult = mrOK) then
    begin
      dm.tblDogovoriMedUslugi.FullRefresh;
    end;
  frmMedUslugii.Free;
end;

procedure TfrmDogovoriMedUslugi.aDodadiPaketExecute(Sender: TObject);
begin
  frmSanPaketi:=TfrmSanPaketi.Create(self,false);
  frmSanPaketi.ShowModal;
  if (frmSanPaketi.ModalResult = mrOK) then
    begin
      dm.tblDogovoriSanPaketi.FullRefresh;
    end;
  frmSanPaketi.Free;
end;

procedure TfrmDogovoriMedUslugi.aDodadiSanUslugiExecute(Sender: TObject);
begin
  frmSanUslugi:=TfrmSanUslugi.Create(self,false);
  frmSanUslugi.ShowModal;
  if (frmSanUslugi.ModalResult = mrOK) then
    begin
      dm.tblDogovoriSanUslugi.FullRefresh;
    end;
  frmSanUslugi.Free;
end;

//  ����� �� ����� �� ����� �� ������������� �� �������� � ��������������� ��
//  �������(������ ���������� ��������)
procedure TfrmDogovoriMedUslugi.aFormConfigExecute(Sender: TObject);
begin
  frmFormConfig := TfrmFormConfig.Create(Application);
  frmFormConfig.formPtr := Addr(Self);
  frmFormConfig.ShowModal;
  frmFormConfig.Free;
end;

//----------------------------------------------------------------------------------
// ���� -> ��������� �� ��������, ���������� �� ��������� � �������� �� Print ��������
//----------------------------------------------------------------------------------

// ��������� �� ���������� �� ��������� � �������� �� Print �������� �� .ini ��������
procedure TfrmDogovoriMedUslugi.zacuvajPrintVoIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmCreate);
  try
    AStream.WriteComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;

end;

// ��������� �� ������ �� ��������� � �������� �� Print �������� �������� �� .ini ��������
procedure TfrmDogovoriMedUslugi.procitajPrintOdIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmOpenRead);
  try
    dxComponentPrinter1Link1.RestoreDefaults;
    AStream.ReadComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;
end;

procedure TfrmDogovoriMedUslugi.ZemiImeDvoenKluc(tip:TcxDBTextEdit; sifra:TcxDBTextEdit; lukap:TcxExtLookupComboBox);
begin

  if (tip.Text <>'') and (sifra.Text<>'')  then
  begin
      lukap.EditValue := VarArrayOf([ StrToInt(tip.Text), StrToInt(sifra.Text)]);
  end
  else
  begin
      lukap.Clear;
  end;
end;



procedure TfrmDogovoriMedUslugi.proveriDatum(datum: TcxDBDateEdit);
begin
   if ((TIP_PARTNER.Text <> '') and (PARTNER.Text <> '') and (DATUM_OD.Text <> '') and (DATUM_DO.Text <> '')) then
      begin
        dm.qAktivenDog.Close;
        dm.qAktivenDog.ParamByName('datum_od').Value:=dm.tblDogovoriDATUM_OD.Value;
        dm.qAktivenDog.ParamByName('datum_do').Value:=dm.tblDogovoriDATUM_DO.Value;
        dm.qAktivenDog.ParamByName('tip_partner').Value:=dm.tblDogovoriTIP_PARTNER.Value;
        dm.qAktivenDog.ParamByName('partner').Value:=dm.tblDogovoriPARTNER.Value;
        dm.qAktivenDog.ExecQuery;
        if (((dm.qAktivenDog.FldByName['br'].Value = 1) and (cxGrid1DBTableView1.DataController.DataSource.State = dsInsert)) or
            ((dm.qAktivenDog.FldByName['br'].Value > 1)and (cxGrid1DBTableView1.DataController.DataSource.State = dsEdit)))then
           begin
             ShowMessage('��� ������������ �� ������ �� ������� �� ��������� �������!!!');
             DATUM_OD.Clear;
             DATUM_DO.Clear;
           end;
      end;
end;

procedure TfrmDogovoriMedUslugi.proveriDatumDogOrgan(datum: TcxDBDateEdit);
begin
   if ((TIP_PARTNER.Text <> '') and (PARTNER.Text <> '') and (DATUM_OD.Text <> '') and (DATUM_DO.Text <> '')) then
      begin
        dm.qAktivenDog.Close;
        dm.qAktivenDog.ParamByName('datum_od').Value:=dm.tblDogovoriDATUM_OD.Value;
        dm.qAktivenDog.ParamByName('datum_do').Value:=dm.tblDogovoriDATUM_DO.Value;
        dm.qAktivenDog.ParamByName('tip_partner').Value:=dm.tblDogovoriTIP_PARTNER.Value;
        dm.qAktivenDog.ParamByName('partner').Value:=dm.tblDogovoriPARTNER.Value;
        dm.qAktivenDog.ExecQuery;
        if (((dm.qAktivenDog.FldByName['br'].Value = 1) and (cxGrid1DBTableView1.DataController.DataSource.State = dsInsert)) or
            ((dm.qAktivenDog.FldByName['br'].Value > 1)and (cxGrid1DBTableView1.DataController.DataSource.State = dsEdit)))then
           begin
             ShowMessage('��� ������������ �� ������ �� ������� �� ��������� �������!!!');
             DATUM_OD.Clear;
             DATUM_DO.Clear;
           end;
      end;
end;

end.
