object frmPoraka: TfrmPoraka
  Left = 200
  Top = 108
  BorderStyle = bsDialog
  Caption = 'About'
  ClientHeight = 137
  ClientWidth = 273
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = True
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 8
    Top = 8
    Width = 257
    Height = 73
    BevelInner = bvRaised
    BevelOuter = bvLowered
    ParentColor = True
    TabOrder = 0
    object Comments: TLabel
      Left = 16
      Top = 16
      Width = 211
      Height = 39
      Caption = 
        #1044#1086#1075#1086#1074#1086#1088#1086#1090' '#1085#1077#1084#1086#1078#1077' '#1076#1072' '#1089#1077' '#1072#1078#1091#1088#1080#1088#1072', '#1076#1086#1075#1086#1074#1086#1088#1086#1090' '#1077' '#1087#1088#1077#1074#1079#1077#1084#1077#1085' '#1074#1086' '#1087#1088#1077#1089#1084#1077#1090 +
        #1082#1072' '#1085#1072' '#1094#1077#1085#1072' '#1079#1072' '#1089#1072#1085#1080#1090#1072#1088#1085#1080' '#1091#1089#1083#1091#1075#1080' !!!'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      WordWrap = True
      IsControl = True
    end
  end
  object OKButton: TButton
    Left = 95
    Top = 95
    Width = 75
    Height = 25
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 1
    OnClick = OKButtonClick
  end
end
