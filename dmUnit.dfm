object dm: Tdm
  OldCreateOrder = False
  Height = 615
  Width = 652
  object tblDogovori: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE DOG_DOGOVORI'
      'SET '
      '    ID = :ID,'
      '    BROJ = :BROJ,'
      '    DATUM_OD = :DATUM_OD,'
      '    DATUM_DO = :DATUM_DO,'
      '    TIP_PARTNER = :TIP_PARTNER,'
      '    PARTNER = :PARTNER,'
      '    OPIS = :OPIS,'
      '    CUSTOM1 = :CUSTOM1,'
      '    CUSTOM2 = :CUSTOM2,'
      '    CUSTOM3 = :CUSTOM3,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD,'
      '    CENA_IDENTIFIKACIJA = :CENA_IDENTIFIKACIJA,'
      '    CENA_ANTIBIOGRAM = :CENA_ANTIBIOGRAM,'
      '    BROJ_DENOVI_FAKTURA = :BROJ_DENOVI_FAKTURA'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    DOG_DOGOVORI'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO DOG_DOGOVORI('
      '    ID,'
      '    BROJ,'
      '    DATUM_OD,'
      '    DATUM_DO,'
      '    TIP_PARTNER,'
      '    PARTNER,'
      '    OPIS,'
      '    CUSTOM1,'
      '    CUSTOM2,'
      '    CUSTOM3,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD,'
      '    CENA_IDENTIFIKACIJA,'
      '    CENA_ANTIBIOGRAM,'
      '    BROJ_DENOVI_FAKTURA'
      ')'
      'VALUES('
      '    :ID,'
      '    :BROJ,'
      '    :DATUM_OD,'
      '    :DATUM_DO,'
      '    :TIP_PARTNER,'
      '    :PARTNER,'
      '    :OPIS,'
      '    :CUSTOM1,'
      '    :CUSTOM2,'
      '    :CUSTOM3,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD,'
      '    :CENA_IDENTIFIKACIJA,'
      '    :CENA_ANTIBIOGRAM,'
      '    :BROJ_DENOVI_FAKTURA'
      ')')
    RefreshSQL.Strings = (
      'select d.id,'
      '       d.broj,'
      '       d.datum_od,'
      '       d.datum_do,'
      '       d.tip_partner,'
      '       d.partner,'
      '       mp.naziv,'
      '       mp.naziv_skraten,'
      '       d.opis,'
      '       d.custom1,'
      '       d.custom2,'
      '       d.custom3,'
      '       d.ts_ins,'
      '       d.ts_upd,'
      '       d.usr_ins,'
      '       d.usr_upd,'
      '       d.cena_identifikacija,'
      '       d.cena_antibiogram,'
      '       d.broj_denovi_faktura'
      'from dog_dogovori d'
      
        'inner join mat_partner mp on mp.id=d.partner and mp.tip_partner ' +
        '= d.tip_partner'
      ''
      ' WHERE '
      '        D.ID = :OLD_ID'
      '    ')
    SelectSQL.Strings = (
      'select d.id,'
      '       d.broj,'
      '       d.datum_od,'
      '       d.datum_do,'
      '       d.tip_partner,'
      '       d.partner,'
      '       mp.naziv,'
      '       mp.naziv_skraten,'
      '       d.opis,'
      '       d.custom1,'
      '       d.custom2,'
      '       d.custom3,'
      '       d.ts_ins,'
      '       d.ts_upd,'
      '       d.usr_ins,'
      '       d.usr_upd,'
      '       d.cena_identifikacija,'
      '       d.cena_antibiogram,'
      '       d.broj_denovi_faktura'
      'from dog_dogovori d'
      
        'inner join mat_partner mp on mp.id=d.partner and mp.tip_partner ' +
        '= d.tip_partner'
      'order by d.datum_od, d.datum_do')
    AutoUpdateOptions.UpdateTableName = 'DOG_DOGOVORI'
    AutoUpdateOptions.KeyFields = 'ID'
    AutoUpdateOptions.AutoReWriteSqls = True
    AutoUpdateOptions.GeneratorName = 'GEN_DOG_DOGOVORI_ID'
    AutoUpdateOptions.WhenGetGenID = wgBeforePost
    BeforeDelete = TabelaBeforeDelete
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 48
    Top = 24
    object tblDogovoriID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblDogovoriBROJ: TFIBStringField
      DisplayLabel = #1041#1088#1086#1112
      FieldName = 'BROJ'
      Size = 15
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDogovoriTIP_PARTNER: TFIBIntegerField
      DisplayLabel = #1044#1086#1075#1086#1074#1086#1088#1077#1085' '#1086#1088#1075#1072#1085' - '#1090#1080#1087
      FieldName = 'TIP_PARTNER'
    end
    object tblDogovoriPARTNER: TFIBIntegerField
      DisplayLabel = #1044#1086#1075#1086#1074#1086#1088#1077#1085' '#1086#1088#1075#1072#1085' - '#1096#1080#1092#1088#1072
      FieldName = 'PARTNER'
    end
    object tblDogovoriNAZIV: TFIBStringField
      DisplayLabel = #1044#1086#1075#1086#1074#1086#1088#1077#1085' '#1086#1088#1075#1072#1085' - '#1085#1072#1079#1080#1074
      FieldName = 'NAZIV'
      Size = 255
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDogovoriNAZIV_SKRATEN: TFIBStringField
      DisplayLabel = #1044#1086#1075#1086#1074#1086#1088#1077#1085' '#1086#1088#1075#1072#1085' - '#1089#1082#1088#1072#1090#1077#1085' '#1085#1072#1079#1080#1074
      FieldName = 'NAZIV_SKRATEN'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDogovoriOPIS: TFIBStringField
      DisplayLabel = #1047#1072#1073#1077#1083#1077#1096#1082#1072
      FieldName = 'OPIS'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDogovoriCUSTOM1: TFIBStringField
      FieldName = 'CUSTOM1'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDogovoriCUSTOM2: TFIBStringField
      FieldName = 'CUSTOM2'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDogovoriCUSTOM3: TFIBStringField
      FieldName = 'CUSTOM3'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDogovoriTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_INS'
      DisplayFormat = 'dd.mm.yyyy hh:mm AMPM'
    end
    object tblDogovoriTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_UPD'
      DisplayFormat = 'dd.mm.yyyy hh:mm AMPM'
    end
    object tblDogovoriUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDogovoriUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDogovoriDATUM_OD: TFIBDateField
      DisplayLabel = #1044#1072#1090#1091#1084' '#1054#1076
      FieldName = 'DATUM_OD'
      DisplayFormat = 'dd.mm.yyyy'
    end
    object tblDogovoriDATUM_DO: TFIBDateField
      DisplayLabel = #1044#1072#1090#1091#1084' '#1044#1086
      FieldName = 'DATUM_DO'
      DisplayFormat = 'dd.mm.yyyy'
    end
    object tblDogovoriCENA_IDENTIFIKACIJA: TFIBBCDField
      DisplayLabel = #1048#1076#1077#1085#1090#1080#1092#1080#1082#1072#1094#1080#1112#1072' '#1085#1072' '#1073#1072#1082#1090#1077#1088#1080#1080' '#1072#1074#1090#1086#1084#1072#1090#1089#1082#1080
      FieldName = 'CENA_IDENTIFIKACIJA'
      Size = 2
    end
    object tblDogovoriCENA_ANTIBIOGRAM: TFIBBCDField
      DisplayLabel = #1040#1085#1090#1080#1073#1080#1086#1075#1088#1072#1084' '#1082#1083#1072#1089#1080#1095#1077#1085
      FieldName = 'CENA_ANTIBIOGRAM'
      Size = 2
    end
    object tblDogovoriBROJ_DENOVI_FAKTURA: TFIBSmallIntField
      DisplayLabel = #1041#1088#1086#1112' '#1085#1072' '#1076#1077#1085#1086#1074#1080' '#1079#1072' '#1076#1086#1089#1087#1077#1074#1072#1114#1077
      FieldName = 'BROJ_DENOVI_FAKTURA'
    end
  end
  object dsDogovori: TDataSource
    DataSet = tblDogovori
    Left = 160
    Top = 32
  end
  object dsDogovorniOrgani: TDataSource
    DataSet = tblDogovorniOrgani
    Left = 160
    Top = 96
  end
  object tblDogovorniOrgani: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE DOG_DOGOVOREN_ORGAN'
      'SET '
      '    ID = :ID,'
      '    DOGOVOR_ID = :DOGOVOR_ID,'
      '    TIP_PARTNER = :TIP_PARTNER,'
      '    PARTNER = :PARTNER,'
      '    OPIS = :OPIS,'
      '    CUSTOM1 = :CUSTOM1,'
      '    CUSTOM2 = :CUSTOM2,'
      '    CUSTOM3 = :CUSTOM3,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    DOG_DOGOVOREN_ORGAN'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO DOG_DOGOVOREN_ORGAN('
      '    ID,'
      '    DOGOVOR_ID,'
      '    TIP_PARTNER,'
      '    PARTNER,'
      '    OPIS,'
      '    CUSTOM1,'
      '    CUSTOM2,'
      '    CUSTOM3,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD'
      ')'
      'VALUES('
      '    :ID,'
      '    :DOGOVOR_ID,'
      '    :TIP_PARTNER,'
      '    :PARTNER,'
      '    :OPIS,'
      '    :CUSTOM1,'
      '    :CUSTOM2,'
      '    :CUSTOM3,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD'
      ')')
    RefreshSQL.Strings = (
      'select d.id,'
      '       d.dogovor_id,'
      '       d.tip_partner,'
      '       d.partner,'
      '       mp.naziv,'
      '       mp.naziv_skraten,'
      '       d.opis,'
      '       d.custom1,'
      '       d.custom2,'
      '       d.custom3,'
      '       d.ts_ins,'
      '       d.ts_upd,'
      '       d.usr_ins,'
      '       d.usr_upd'
      'from dog_dogovoren_organ d'
      
        'inner join mat_partner mp on mp.tip_partner = d.partner and mp.i' +
        'd = d.partner'
      'where(  d.dogovor_id = :mas_id'
      '     ) and (     D.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'select d.id,'
      '       d.dogovor_id,'
      '       d.tip_partner,'
      '       d.partner,'
      '       mp.naziv,'
      '       mp.naziv_skraten,'
      '       d.opis,'
      '       d.custom1,'
      '       d.custom2,'
      '       d.custom3,'
      '       d.ts_ins,'
      '       d.ts_upd,'
      '       d.usr_ins,'
      '       d.usr_upd'
      'from dog_dogovoren_organ d'
      
        'inner join mat_partner mp on mp.tip_partner = d.tip_partner and ' +
        'mp.id = d.partner'
      'where d.dogovor_id = :mas_id')
    AutoUpdateOptions.UpdateTableName = 'DOG_DOGOVOREN_ORGAN'
    AutoUpdateOptions.KeyFields = 'ID'
    AutoUpdateOptions.AutoReWriteSqls = True
    AutoUpdateOptions.GeneratorName = 'GEN_DOG_DOGOVOREN_ORGAN_ID'
    AutoUpdateOptions.WhenGetGenID = wgBeforePost
    BeforeDelete = TabelaBeforeDelete
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    DataSource = dsDogovori
    Left = 48
    Top = 96
    object tblDogovorniOrganiID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblDogovorniOrganiDOGOVOR_ID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072' '#1085#1072' '#1076#1086#1075#1086#1074#1086#1088
      FieldName = 'DOGOVOR_ID'
    end
    object tblDogovorniOrganiTIP_PARTNER: TFIBIntegerField
      DisplayLabel = #1044#1086#1075#1086#1074#1086#1088#1077#1085' '#1086#1088#1075#1072#1085' - '#1090#1080#1087
      FieldName = 'TIP_PARTNER'
    end
    object tblDogovorniOrganiPARTNER: TFIBIntegerField
      DisplayLabel = #1044#1086#1075#1086#1074#1086#1088#1077#1085' '#1086#1088#1075#1072#1085' - '#1096#1080#1092#1088#1072
      FieldName = 'PARTNER'
    end
    object tblDogovorniOrganiNAZIV: TFIBStringField
      DisplayLabel = #1044#1086#1075#1086#1074#1086#1088#1077#1085' '#1086#1088#1075#1072#1085' - '#1085#1072#1079#1080#1074
      FieldName = 'NAZIV'
      Size = 255
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDogovorniOrganiNAZIV_SKRATEN: TFIBStringField
      DisplayLabel = #1044#1086#1075#1086#1074#1086#1088#1077#1085' '#1086#1088#1075#1072#1085' - '#1089#1082#1088#1072#1090#1077#1085' '#1085#1072#1079#1080#1074
      FieldName = 'NAZIV_SKRATEN'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDogovorniOrganiOPIS: TFIBStringField
      DisplayLabel = #1047#1072#1073#1077#1083#1077#1096#1082#1072
      FieldName = 'OPIS'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDogovorniOrganiCUSTOM1: TFIBStringField
      FieldName = 'CUSTOM1'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDogovorniOrganiCUSTOM2: TFIBStringField
      FieldName = 'CUSTOM2'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDogovorniOrganiCUSTOM3: TFIBStringField
      FieldName = 'CUSTOM3'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDogovorniOrganiTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_INS'
      DisplayFormat = 'dd.mm.yyyy hh:mm AMPM'
    end
    object tblDogovorniOrganiTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_UPD'
      DisplayFormat = 'dd.mm.yyyy hh:mm AMPM'
    end
    object tblDogovorniOrganiUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDogovorniOrganiUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dslSANPaketi: TDataSource
    DataSet = tblSANPaketi
    Left = 160
    Top = 160
  end
  object tblSANPaketi: TpFIBDataSet
    SelectSQL.Strings = (
      'select su.id,'
      '        su.naziv,'
      '        su.cena,'
      '        su.kontrolen_pregled,'
      '        su.ts_ins,'
      '        su.ts_upd,'
      '        su.usr_ins,'
      '        su.usr_upd'
      'from san_paketi su'
      
        'left outer join dog_san_paketi sp on sp.paket = su.id and sp.dog' +
        'ovor_id = :mas_id'
      'where sp.id is null'
      'order by id')
    BeforeDelete = TabelaBeforeDelete
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    DataSource = dsDogovori
    Left = 48
    Top = 160
    object tblSANPaketiID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblSANPaketiNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'NAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblSANPaketiCENA: TFIBBCDField
      DisplayLabel = #1062#1077#1085#1072
      FieldName = 'CENA'
      Size = 2
    end
    object tblSANPaketiTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_INS'
    end
    object tblSANPaketiTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_UPD'
    end
    object tblSANPaketiUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblSANPaketiUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblSANPaketiKONTROLEN_PREGLED: TFIBSmallIntField
      DisplayLabel = #1050#1086#1085#1090#1088#1086#1083#1077#1085' '#1087#1088#1077#1075#1083#1077#1076
      FieldName = 'KONTROLEN_PREGLED'
    end
  end
  object tblDogovoriSanPaketi: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE DOG_SAN_PAKETI'
      'SET '
      '    ID = :ID,'
      '    DOGOVOR_ID = :DOGOVOR_ID,'
      '    PAKET = :PAKET,'
      '    CENA = :CENA,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD,'
      '    CUSTOM1 = :CUSTOM1,'
      '    CUSTOM2 = :CUSTOM2,'
      '    CUSTOM3 = :CUSTOM3'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    DOG_SAN_PAKETI'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO DOG_SAN_PAKETI('
      '    ID,'
      '    DOGOVOR_ID,'
      '    PAKET,'
      '    CENA,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD,'
      '    CUSTOM1,'
      '    CUSTOM2,'
      '    CUSTOM3'
      ')'
      'VALUES('
      '    :ID,'
      '    :DOGOVOR_ID,'
      '    :PAKET,'
      '    :CENA,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD,'
      '    :CUSTOM1,'
      '    :CUSTOM2,'
      '    :CUSTOM3'
      ')')
    RefreshSQL.Strings = (
      'select sp.id,'
      '       sp.dogovor_id,'
      '       sp.paket,'
      '       p.naziv as paketNaziv,'
      '       sp.cena,'
      '       sp.ts_ins,'
      '       sp.ts_upd,'
      '       sp.usr_ins,'
      '       sp.usr_upd,'
      '       sp.custom1,'
      '       sp.custom2,'
      '       sp.custom3'
      'from dog_san_paketi sp'
      'inner join san_paketi p on p.id = sp.paket'
      'where(  sp.dogovor_id = :mas_id'
      '     ) and (     SP.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'select sp.id,'
      '       sp.dogovor_id,'
      '       sp.paket,'
      '       p.naziv as paketNaziv,'
      '       sp.cena,'
      '       sp.ts_ins,'
      '       sp.ts_upd,'
      '       sp.usr_ins,'
      '       sp.usr_upd,'
      '       sp.custom1,'
      '       sp.custom2,'
      '       sp.custom3'
      'from dog_san_paketi sp'
      'inner join san_paketi p on p.id = sp.paket'
      'where sp.dogovor_id = :mas_id')
    AutoUpdateOptions.UpdateTableName = 'DOG_SAN_PAKETI'
    AutoUpdateOptions.KeyFields = 'ID'
    AutoUpdateOptions.AutoReWriteSqls = True
    AutoUpdateOptions.GeneratorName = 'GEN_DOG_SAN_PAKETI_ID'
    AutoUpdateOptions.WhenGetGenID = wgBeforePost
    BeforeDelete = TabelaBeforeDelete
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    DataSource = dsDogovori
    Left = 48
    Top = 232
    object tblDogovoriSanPaketiID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblDogovoriSanPaketiDOGOVOR_ID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072' '#1085#1072' '#1076#1086#1075#1086#1074#1086#1088
      FieldName = 'DOGOVOR_ID'
    end
    object tblDogovoriSanPaketiCENA: TFIBFloatField
      DisplayLabel = #1062#1077#1085#1072
      FieldName = 'CENA'
      OnSetText = tblDogovoriSanPaketiCENASetText
    end
    object tblDogovoriSanPaketiTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_INS'
    end
    object tblDogovoriSanPaketiTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_UPD'
    end
    object tblDogovoriSanPaketiUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDogovoriSanPaketiUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDogovoriSanPaketiCUSTOM1: TFIBStringField
      FieldName = 'CUSTOM1'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDogovoriSanPaketiCUSTOM2: TFIBStringField
      FieldName = 'CUSTOM2'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDogovoriSanPaketiCUSTOM3: TFIBStringField
      FieldName = 'CUSTOM3'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDogovoriSanPaketiPAKET: TFIBIntegerField
      DisplayLabel = #1055#1072#1082#1077#1090' '#1084#1072' '#1091#1089#1083#1091#1075#1080' - '#1096#1080#1092#1088#1072
      FieldName = 'PAKET'
    end
    object tblDogovoriSanPaketiPAKETNAZIV: TFIBStringField
      DisplayLabel = #1055#1072#1082#1077#1090' '#1085#1072' '#1091#1089#1083#1091#1075#1080
      FieldName = 'PAKETNAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsDogovoriSanPaketi: TDataSource
    DataSet = tblDogovoriSanPaketi
    Left = 168
    Top = 232
  end
  object qAktivenDog: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select count(d.id) br'
      'from dog_dogovori d'
      'where d.partner = :partner and d.tip_partner = :tip_partner and'
      
        '      ((d.datum_od between :datum_od and :datum_do) or (d.datum_' +
        'do between :datum_od and :datum_do))')
    Left = 328
    Top = 32
  end
  object qAktivenDogovorDo: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select count(dd.id) br'
      'from dog_dogovoren_organ dd'
      'inner join dog_dogovori d on d.id = dd.dogovor_id'
      
        'where dd.partner = :partner and dd.tip_partner = :tip_partner an' +
        'd'
      
        '      ((d.datum_od between :datum_od and :datum_do) or (d.datum_' +
        'do between :datum_od and :datum_do))')
    Left = 328
    Top = 88
  end
  object tblMEDUslugi: TpFIBDataSet
    SelectSQL.Strings = (
      'select mdu.id,'
      '       mdu.re,'
      '       mdu.naziv,'
      '       mdu.besplatna,'
      '       mdu.cena_fzo,'
      '       mdu.re_naplata,'
      '       mdu.sifra,'
      '       mdu.def_analiza,'
      '       mdu.postapka,'
      '       mdu.aktivna,'
      '       mdu.br_analizi,'
      '       mdu.grupa,'
      '       mdu.kratenka,'
      '       mdu.primerok,'
      '       mdu.grupa_cena'
      'from med_def_uslugi mdu')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    Left = 48
    Top = 304
    object tblMEDUslugiID: TFIBSmallIntField
      DisplayLabel = #1064#1080#1092#1088#1072' - '#1090#1077#1093#1085#1080#1095#1082#1080' '#1082#1083#1091#1095
      FieldName = 'ID'
    end
    object tblMEDUslugiRE: TFIBIntegerField
      DisplayLabel = #1056#1072#1073'. '#1077#1076#1080#1085#1080#1094#1072' - '#1096#1080#1092#1088#1072
      FieldName = 'RE'
    end
    object tblMEDUslugiNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'NAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblMEDUslugiBESPLATNA: TFIBStringField
      FieldName = 'BESPLATNA'
      Size = 1
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblMEDUslugiCENA_FZO: TFIBFloatField
      DisplayLabel = #1062#1077#1085#1072
      FieldName = 'CENA_FZO'
    end
    object tblMEDUslugiRE_NAPLATA: TFIBIntegerField
      FieldName = 'RE_NAPLATA'
    end
    object tblMEDUslugiSIFRA: TFIBStringField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'SIFRA'
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblMEDUslugiDEF_ANALIZA: TFIBSmallIntField
      FieldName = 'DEF_ANALIZA'
    end
    object tblMEDUslugiPOSTAPKA: TFIBSmallIntField
      FieldName = 'POSTAPKA'
    end
    object tblMEDUslugiAKTIVNA: TFIBSmallIntField
      FieldName = 'AKTIVNA'
    end
    object tblMEDUslugiBR_ANALIZI: TFIBSmallIntField
      FieldName = 'BR_ANALIZI'
    end
    object tblMEDUslugiGRUPA: TFIBIntegerField
      FieldName = 'GRUPA'
    end
    object tblMEDUslugiKRATENKA: TFIBStringField
      FieldName = 'KRATENKA'
      Size = 10
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblMEDUslugiPRIMEROK: TFIBIntegerField
      FieldName = 'PRIMEROK'
    end
    object tblMEDUslugiGRUPA_CENA: TFIBStringField
      FieldName = 'GRUPA_CENA'
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsMEDUslugi: TDataSource
    DataSet = tblMEDUslugi
    Left = 168
    Top = 304
  end
  object tblDogovoriMedUslugi: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE DOG_MED_USLUGI'
      'SET '
      '    ID = :ID,'
      '    DOGOVOR_ID = :DOGOVOR_ID,'
      '    USLUGA = :USLUGA,'
      '    CENA = :CENA,'
      '    CUSTOM1 = :CUSTOM1,'
      '    CUSTOM2 = :CUSTOM2,'
      '    CUSTOM3 = :CUSTOM3,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    DOG_MED_USLUGI'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO DOG_MED_USLUGI('
      '    ID,'
      '    DOGOVOR_ID,'
      '    USLUGA,'
      '    CENA,'
      '    CUSTOM1,'
      '    CUSTOM2,'
      '    CUSTOM3,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD'
      ')'
      'VALUES('
      '    :ID,'
      '    :DOGOVOR_ID,'
      '    :USLUGA,'
      '    :CENA,'
      '    :CUSTOM1,'
      '    :CUSTOM2,'
      '    :CUSTOM3,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD'
      ')')
    RefreshSQL.Strings = (
      'select mu.id,'
      '       mu.dogovor_id,'
      '       mu.usluga,'
      '       du.naziv as usluga_naziv,'
      '       du.sifra as usluga_sifra,'
      '       mu.cena,'
      '       mu.custom1,'
      '       mu.custom2,'
      '       mu.custom3,'
      '       mu.ts_ins,'
      '       mu.ts_upd,'
      '       mu.usr_ins,'
      '       mu.usr_upd'
      'from dog_med_uslugi mu'
      'inner join med_def_uslugi du on du.id = mu.usluga'
      'where(  mu.dogovor_id = :mas_id'
      '     ) and (     MU.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'select mu.id,'
      '       mu.dogovor_id,'
      '       mu.usluga,'
      '       du.naziv as usluga_naziv,'
      '       du.sifra as usluga_sifra,'
      '       mu.cena,'
      '       mu.custom1,'
      '       mu.custom2,'
      '       mu.custom3,'
      '       mu.ts_ins,'
      '       mu.ts_upd,'
      '       mu.usr_ins,'
      '       mu.usr_upd'
      'from dog_med_uslugi mu'
      'inner join med_def_uslugi du on du.id = mu.usluga'
      'where mu.dogovor_id = :mas_id')
    AutoUpdateOptions.UpdateTableName = 'DOG_MED_USLUGI'
    AutoUpdateOptions.AutoReWriteSqls = True
    AutoUpdateOptions.GeneratorName = 'GEN_DOG_MED_USLUGI_ID'
    AutoUpdateOptions.WhenGetGenID = wgOnNewRecord
    BeforeDelete = TabelaBeforeDelete
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    DataSource = dsDogovori
    Left = 48
    Top = 368
    object tblDogovoriMedUslugiID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblDogovoriMedUslugiDOGOVOR_ID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072' '#1085#1072' '#1076#1086#1075#1086#1074#1086#1088
      FieldName = 'DOGOVOR_ID'
    end
    object tblDogovoriMedUslugiUSLUGA: TFIBIntegerField
      DisplayLabel = #1059#1089#1083#1091#1075#1072' - '#1096#1080#1092#1088#1072
      FieldName = 'USLUGA'
    end
    object tblDogovoriMedUslugiUSLUGA_NAZIV: TFIBStringField
      DisplayLabel = #1059#1089#1083#1091#1075#1072
      FieldName = 'USLUGA_NAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDogovoriMedUslugiCENA: TFIBFloatField
      DisplayLabel = #1062#1077#1085#1072
      FieldName = 'CENA'
      OnSetText = tblDogovoriMedUslugiCENASetText
    end
    object tblDogovoriMedUslugiCUSTOM1: TFIBStringField
      FieldName = 'CUSTOM1'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDogovoriMedUslugiCUSTOM2: TFIBStringField
      FieldName = 'CUSTOM2'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDogovoriMedUslugiCUSTOM3: TFIBStringField
      FieldName = 'CUSTOM3'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDogovoriMedUslugiTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_INS'
    end
    object tblDogovoriMedUslugiTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_UPD'
    end
    object tblDogovoriMedUslugiUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDogovoriMedUslugiUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDogovoriMedUslugiUSLUGA_SIFRA: TFIBStringField
      DisplayLabel = #1064#1080#1092#1088#1072' '#1085#1072' '#1091#1089#1083#1091#1075#1072
      FieldName = 'USLUGA_SIFRA'
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsDogovoriMedUslugi: TDataSource
    DataSet = tblDogovoriMedUslugi
    Left = 168
    Top = 368
  end
  object tblValuta: TpFIBDataSet
    SelectSQL.Strings = (
      'select mv.id, mv.naziv, mv.kurs'
      'from mat_valuta mv')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    Left = 40
    Top = 552
    object tblValutaID: TFIBStringField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
      Size = 3
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblValutaNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'NAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblValutaKURS: TFIBFloatField
      DisplayLabel = #1050#1091#1088#1089
      FieldName = 'KURS'
    end
  end
  object dsValuta: TDataSource
    DataSet = tblValuta
    Left = 168
    Top = 552
  end
  object qCountDogovorVoSan: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select count(p.id) br'
      'from san_pregled p'
      'where p.dogovor_id = :dogovor_id')
    Left = 328
    Top = 152
  end
  object qCountDogovorVoMED: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select count(p.id) br'
      'from med_uslugi p'
      'where p.dogovor_id = :dogovor_id')
    Left = 328
    Top = 224
  end
  object tblSANUslugi: TpFIBDataSet
    SelectSQL.Strings = (
      'select s.id,'
      '       s.naziv,'
      '       s.cena,'
      '       s.re,'
      '       r.naziv re_naziv,'
      '       s.status,'
      '       s.tip,'
      '       s.ts_ins,'
      '       s.ts_upd,'
      '       s.usr_ins,'
      '       s.usr_upd'
      'from san_uslugi s'
      
        'left outer join dog_san_uslugi su on su.usluga_id = s.id and su.' +
        'dogovor_id = :mas_id'
      'inner join mat_re r on r.id = s.re'
      'where su.id is null'
      'order by s.tip,s.id')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    Left = 48
    Top = 432
    object tblSANUslugiID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblSANUslugiNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'NAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblSANUslugiCENA: TFIBBCDField
      DisplayLabel = #1062#1077#1085#1072
      FieldName = 'CENA'
      Size = 2
    end
    object tblSANUslugiRE: TFIBIntegerField
      DisplayLabel = #1051#1072#1073#1086#1088#1072#1090#1086#1088#1080#1112#1072' - '#1096#1080#1092#1088#1072
      FieldName = 'RE'
    end
    object tblSANUslugiSTATUS: TFIBSmallIntField
      DisplayLabel = #1057#1090#1072#1090#1091#1089
      FieldName = 'STATUS'
    end
    object tblSANUslugiTIP: TFIBSmallIntField
      DisplayLabel = #1058#1080#1087
      FieldName = 'TIP'
    end
    object tblSANUslugiTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_INS'
    end
    object tblSANUslugiTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_UPD'
    end
    object tblSANUslugiUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblSANUslugiUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblSANUslugiRE_NAZIV: TFIBStringField
      DisplayLabel = #1051#1072#1073#1086#1088#1072#1090#1086#1088#1080#1112#1072
      FieldName = 'RE_NAZIV'
      Size = 255
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsSANUslugi: TDataSource
    DataSet = tblSANUslugi
    Left = 168
    Top = 432
  end
  object tblDogovoriSanUslugi: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE DOG_SAN_USLUGI'
      'SET '
      '    DOGOVOR_ID = :DOGOVOR_ID,'
      '    USLUGA_ID = :USLUGA_ID,'
      '    CENA = :CENA,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD,'
      '    CUSTOM1 = :CUSTOM1,'
      '    CUSTOM2 = :CUSTOM2,'
      '    CUSTOM3 = :CUSTOM3'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    DOG_SAN_USLUGI'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO DOG_SAN_USLUGI('
      '    ID,'
      '    DOGOVOR_ID,'
      '    USLUGA_ID,'
      '    CENA,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD,'
      '    CUSTOM1,'
      '    CUSTOM2,'
      '    CUSTOM3'
      ')'
      'VALUES('
      '    :ID,'
      '    :DOGOVOR_ID,'
      '    :USLUGA_ID,'
      '    :CENA,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD,'
      '    :CUSTOM1,'
      '    :CUSTOM2,'
      '    :CUSTOM3'
      ')')
    RefreshSQL.Strings = (
      'select su.id,'
      '       su.dogovor_id,'
      '       su.usluga_id,'
      '       u.naziv as UslugaNaziv,'
      '       u.tip,'
      '       su.cena,'
      '       su.ts_ins,'
      '       su.ts_upd,'
      '       su.usr_ins,'
      '       su.usr_upd,'
      '       su.custom1,'
      '       su.custom2,'
      '       su.custom3'
      'from dog_san_uslugi su'
      'inner join san_uslugi u on u.id = su.usluga_id'
      'where(  su.dogovor_id = :mas_id'
      '     ) and (     SU.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'select su.id,'
      '       su.dogovor_id,'
      '       su.usluga_id,'
      '       u.naziv as UslugaNaziv,'
      '       u.tip,'
      '       su.cena,'
      '       su.ts_ins,'
      '       su.ts_upd,'
      '       su.usr_ins,'
      '       su.usr_upd,'
      '       su.custom1,'
      '       su.custom2,'
      '       su.custom3'
      'from dog_san_uslugi su'
      'inner join san_uslugi u on u.id = su.usluga_id'
      'where su.dogovor_id = :mas_id'
      'order by u.tip, su.id')
    AutoUpdateOptions.UpdateTableName = 'DOG_SAN_USLUGI'
    AutoUpdateOptions.KeyFields = 'ID'
    AutoUpdateOptions.GeneratorName = 'GEN_DOG_SAN_USLUGI_ID'
    AutoUpdateOptions.WhenGetGenID = wgBeforePost
    BeforeDelete = TabelaBeforeDelete
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    DataSource = dsDogovori
    Left = 40
    Top = 496
    object tblDogovoriSanUslugiID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblDogovoriSanUslugiDOGOVOR_ID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072' '#1085#1072' '#1076#1086#1075#1086#1074#1086#1088
      FieldName = 'DOGOVOR_ID'
    end
    object tblDogovoriSanUslugiUSLUGA_ID: TFIBIntegerField
      DisplayLabel = #1059#1089#1083#1091#1075#1072' - '#1096#1080#1092#1088#1072
      FieldName = 'USLUGA_ID'
    end
    object tblDogovoriSanUslugiUSLUGANAZIV: TFIBStringField
      DisplayLabel = #1059#1089#1083#1091#1075#1072' '
      FieldName = 'USLUGANAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDogovoriSanUslugiTIP: TFIBSmallIntField
      DisplayLabel = #1058#1080#1087
      FieldName = 'TIP'
    end
    object tblDogovoriSanUslugiCENA: TFIBFloatField
      DisplayLabel = #1062#1077#1085#1072
      FieldName = 'CENA'
      OnSetText = tblDogovoriSanUslugiCENASetText
    end
    object tblDogovoriSanUslugiTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_INS'
    end
    object tblDogovoriSanUslugiTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_UPD'
    end
    object tblDogovoriSanUslugiUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDogovoriSanUslugiUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDogovoriSanUslugiCUSTOM1: TFIBStringField
      FieldName = 'CUSTOM1'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDogovoriSanUslugiCUSTOM2: TFIBStringField
      FieldName = 'CUSTOM2'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDogovoriSanUslugiCUSTOM3: TFIBStringField
      FieldName = 'CUSTOM3'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsDogovoriSanUslugi: TDataSource
    DataSet = tblDogovoriSanUslugi
    Left = 168
    Top = 496
  end
  object qCountDogovorVoSanPaket: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select count(p.id) br'
      'from san_pregled p'
      'left outer join san_paketi s on s.naziv = p.paket_id'
      'where p.dogovor_id = :dogovor_id  and s.id =:paket_id')
    Left = 328
    Top = 296
  end
  object qCountDogovorVoSanUslugi: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select count(p.id) br'
      'from san_pregled_s p'
      'where p.m_dogovori_id = :dogovor_id and p.usluga_id= :usluga_id')
    Left = 328
    Top = 368
  end
end
